<?php

use app\models\User;
use yii\grid\GridView;
use yii\helpers\Html;

/** @var $this yii\web\View */
/** @var \yii\data\ActiveDataProvider $dataProvider */

?>
<div class="user-index">
    <?=  GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'id',
                'format' => 'html',
                'value' => function(User $model) {
                    return Html::a($model->id, ['index', 'referral_id' => $model->id]);
                }
            ],
            'surname',
            'forename',
            'email',
            'skype',
            'phone',
            [
                'attribute' => 'referral_id',
                'format' => 'html',
                'value' => function(User $model) {
                    return Html::a($model->referral_id, ['index', 'referral_id' => $model->referral_id]);
                }
            ],
            [
//                'label' => Yii::t('app', 'Actions'),
//                'attribute' => 'id',
                'format' => 'html',
                'value' => function(User $model) {
                    $items = [
                        Html::a(' ', ['update', 'id' => $model->id], [
                            'class' => 'fa fa-pencil-square-o',
                            'title' => Yii::t('app', 'Edit')
                        ])
                    ];
                    if ($model->auth) {
                        $items[] = Html::a('', ['/admin/user/login', 'auth' => $model->auth], [
                            'class' => 'fa fa-sign-in',
                            'title' => Yii::t('app', 'Login')
                        ]);
                    }
                    return implode(' ', $items);
                }
            ],
        ]
    ]) ?>
</div>
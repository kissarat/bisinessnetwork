<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LoginForm */
/* @var $form ActiveForm */

$this->title = Yii::t('app', 'Login');
?>
<div class="user-login">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'password')->passwordInput() ?>
    <?= Html::a(Yii::t('app', 'Forgot your password?'), ['/user/request']) ?>
    <?= Html::a(Yii::t('app', 'Sign Up'), ['/user/create']) ?>
    <p>
        <?= Html::submitButton($this->title,['class'=>'btn btn-success']) ?>
    <p>
    <?php ActiveForm::end(); ?>
</div><!-- user-login -->

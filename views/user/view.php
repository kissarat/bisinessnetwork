<?php
/* @var $this yii\web\View */
/* @var $model \app\models\User */

use app\helpers\Country;
use app\widgets\Panel;
use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->id;

function spaces()
{
    return implode(' ', func_get_args());
}

if (Yii::$app->user->can('manage')) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
}

$attributes = [
    [
        'attribute' => 'account',
        'value' => (float)$model->account
    ],
    [
        'attribute' => 'referral_id',
        'format' => 'html',
        'value' => Html::a($model->referral_id,
            ['view', 'referral_id' => $model->referral_id],
            ['class' => 'value'])
    ],
    'email',
    'skype',
    'phone',
    [
        'attribute' => 'country',
        'value' => $model->country ? Country::decode($model->country) : null
    ],
    'postal',
    'city',
    'address',
    [
        'label' => Yii::t('app', 'Confidant'),
        'value' => spaces($model->confidant_forename, $model->confidant_surname, $model->confidant_patronymic)
    ],
];

?>
<div class="user-view table-block">
    <div>
        <div>
            <div>
                <?= spaces($model->forename, $model->surname, $model->patronymic) ?>
            </div>
        </div>
        <div>
            <div>
                <?= $model->id ?>
            </div>
        </div>
    </div>
    <div>
        <div class="attributes">
            <?= DetailView::widget([
                'model' => $model,
                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                'attributes' => $attributes,
                'template' => '<tr><th>{label}</th><td><div>{value}</div></td></tr>'
            ]) ?>
        </div>
        <div class="huylo"></div>
    </div>
    <div>
        <div></div>
        <div></div>
    </div>
</div>

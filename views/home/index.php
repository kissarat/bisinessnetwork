<?php
use app\widgets\Panel;
use yii\bootstrap\Carousel;
use yii\helpers\Html;

?>
<div class="home-index">
<!--    --><?//= 'http://bisinessnetwork1x1.com/invoice/freekassa?SIGN=8d1b193db49ef51e4a5e58d8c8395c46&MERCHANT_ID=24537&AMOUNT=20&intid=228&MERCHANT_ORDER_ID=9&us_login=user2' ?>
    <?= Carousel::widget([
        'items' => [
            '<img src="/images/slide3.jpg"/>',
            '<img src="/images/slide1.jpg"/>',
            '<img src="/images/slide2.jpg"/>',
            '<img src="/images/slide4.jpg"/>',
            '<img src="/images/slide5.jpg"/>',
        ],
        'options'=>[
            'id'=>'carousel-example-generic',
            'data-ride'=>'carousel',
            'class'=>'slide'
        ],
        'controls'=>[Html::decode('<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>'), Html::decode('<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>')]
    ]);
    ?>


        <div class="panel-group">
        <?= Panel::widget([
            'id' => 'about',
            'title' => Yii::t('app', 'Company'),
            'options' => ['class' => 'col-lg-4'],
            'model'=>[
                'content'=>function($data){
                    return strip_tags(substr($data,0,100)."...",['<img>']);
                }
            ]
        ]) ?>
        <?= Panel::widget([
            'id' => 'marketing',
            'title' => Yii::t('app', 'Marketing'),
            'options' => ['class' => 'col-lg-4'],
            'model'=>[
        'content'=>function($data) {
            return strip_tags(substr($data, 0, 100) . "...", ['<img>']);
        }
            ]
        ]) ?>
            <div id="news" class="col-lg-4 panel panel-default">
                <h2><?=Yii::t('app','News')?></h2>
                <?php
                foreach($news as $item){
                    echo '<div class="news-item">';
                    echo '<h3>'.Html::a($item['title'],'/ru/'.$item['id']).'</h3>';
                    echo '</div>';
                }
                ?>
                <div id="news-bottom"></div>
            </div>
    </div>
    <div id="media">
    <div id="globus">
        <script type="text/javascript" src="//ra.revolvermaps.com/0/0/6.js?i=0v109f8htlt&amp;m=7&amp;s=320&amp;c=e63100&amp;cr1=ffffff&amp;f=arial&amp;l=0&amp;bv=90&amp;lx=-420&amp;ly=420&amp;hi=20&amp;he=7&amp;hc=a8ddff&amp;rs=80" async="async"></script>
    </div>
    <div id="youtube">
        <a href="#"><img src="/images/youtube.png"> </a>
    </div>
        <div id="money">
            <h2><?=Yii::t('app','Exchange course')?></h2>
            <p id="value"><!--  START: курс2.рф Widget HTML 1.0-->
            <div id="exchange" style="vertical-align: baseline; padding: 0; line-height: 0;">
                <style>table td:hover {
                        background: none;
                    }

                    ;
                    table td {
                        vertical-align: middle;
                    }

                    table tbody td tr:hover td {
                        background: none
                    }</style>
                <table
                    style="border-collapse: inherit;text-shadow: none;text-align: center; width: 100%;border-spacing: 0px;border-collapse: separate; height: 20px;">
                    <tr style="font-family: Tahoma, Geneva, sans-serif">
                        <td id="null"
                            style="vertical-align: middle!important;background: linear-gradient(to bottom, #5A6EC0, #051C5B);margin-left: 1px">
                            <a title="Курс валют ЦБ РФ на сегодня" href="http://xn--2-stbsei.xn--p1ai/" target="_blank"
                               style="color: #ffffff; text-decoration: initial; cursor: auto;font-size: 14px;">Курс ЦБ
                                РФ</a>
                        </td>
                        <td style="vertical-align: middle!important; width: 30px ;background: linear-gradient(to bottom, #DEAB89, #864033);padding: 0 7px 0 5px; color: #ffffff;font-size: 14px;"
                            id="curentData">13.02.2016
                        </td>
                    </tr>
                </table>
                <table
                    style="border-collapse: inherit;vertical-align: middle;margin: 0; width: 100%;border-collapse: separate;border-spacing: 0px; height: 124px; ">
                    <tr style=" font-family: Tahoma, Geneva, sans-serif; background: linear-gradient(to bottom, #ADACC6, #383C55);">
                        <td style="vertical-align: middle!important;border-top:5px solid #ffffff;padding: 0 3px 0 3px;vertical-align: middle!important;">
                            <a href="http://xn--2-stbsei.xn--p1ai/dollar-moscow" title="Курс Доллара" target="_blank"
                               style="color: #ffffff; text-decoration: initial;margin-right: 6px; cursor: auto;font-weight: bold;font-size: 14px;">
                                RUB
                            </a>
                        </td>
                        <td style="vertical-align: middle!important;border-top:5px solid #ffffff;border-left: 2px solid #ffffff; text-align: center;">
                            <a href="http://xn--2-stbsei.xn--p1ai/dollar-moscow" target="_blank"
                               title="Курс Доллара"><img
                                    style="width: 23px; height: 14px; vertical-align: middle!important;"
                                    alt="Курс Доллара" src="https://upload.wikimedia.org/wikipedia/en/thumb/f/f3/Flag_of_Russia.svg/125px-Flag_of_Russia.svg.png">
                            </a>
                        </td>
                        <td style="vertical-align: middle!important;border-top:5px solid #ffffff;border-left: 2px solid #ffffff;padding-left: 3px;padding-right: 3px; color: #ffffff;font-size: 14px;vertical-align: middle!important;"
                            id="USDtd">00.0000
                        </td>
                        <td class="nothing" style="vertical-align: middle!important;border-top:5px solid #ffffff;border-left: 2px solid #ffffff; text-align: right; padding: 3px 3px;">
                            <img src="" id="USDsrc"
                                 style="border-right: 2px solid transparent;vertical-align: middle!important;"><span
                                style="color:#ffffff;font-size: 12px;" id="USDtm">0.000</span></td>
                    </tr>
                    <tr style=" font-family: Tahoma, Geneva, sans-serif; background: linear-gradient(to bottom, #ADACC6, #383C55);">
                        <td style="vertical-align: middle!important;border-top:5px solid #ffffff;padding: 0 3px 0 3px;vertical-align: middle!important;">
                            <a href="http://xn--2-stbsei.xn--p1ai/euro-moscow" title="Курс Евро" target="_blank"
                               style="color: #ffffff; text-decoration: initial;margin-right: 6px; cursor: auto;font-weight: bold;font-size: 14px;">EUR</a>
                        </td>
                        <td style="vertical-align: middle!important;border-top:5px solid #ffffff;border-left: 2px solid #ffffff; text-align: center;">
                            <a href="http://xn--2-stbsei.xn--p1ai/euro-moscow" target="_blank" title="Курс Евро"><img
                                    style="width: 23px; height: 14px; vertical-align: middle!important;" alt="Курс Евро"
                                    src="http://xn--2-stbsei.xn--p1ai/images/flag2/eur.png"></a></td>
                        <td style="vertical-align: middle!important;border-top:5px solid #ffffff;border-left: 2px solid #ffffff;padding-left: 3px;padding-right: 3px; color: #ffffff;font-size: 14px;vertical-align: middle!important;"
                            id="USDEUR">00.0000
                        </td>
                        <td class="nothing" style="vertical-align: middle!important;border-top:5px solid #ffffff;border-left: 2px solid #ffffff; text-align: right; padding: 3px 3px;">
                            <img src="" id="EURsrc"
                                 style="border-right: 2px solid transparent;vertical-align: middle!important;"><span
                                style="color:#ffffff;font-size: 12px;" id="EURtm">0.000</span></td>
                    </tr>
                    <tr style=" font-family: Tahoma, Geneva, sans-serif; background: linear-gradient(to bottom, #ADACC6, #383C55);">
                        <td style="vertical-align: middle!important;border-top:5px solid #ffffff;padding: 0 3px 0 3px;vertical-align: middle!important;">
                            <a href="http://xn--2-stbsei.xn--p1ai/pound_sterling-moscow" title="Курс Фунта"
                               target="_blank"
                               style="color: #ffffff; text-decoration: initial;margin-right: 6px; cursor: auto;font-weight: bold;font-size: 14px;">GBP</a>
                        </td>
                        <td style="vertical-align: middle!important;border-top:5px solid #ffffff;border-left: 2px solid #ffffff; text-align: center;">
                            <a href="http://xn--2-stbsei.xn--p1ai/pound_sterling-moscow" target="_blank"
                               title="Курс Фунта"><img
                                    style="width: 23px; height: 14px; vertical-align: middle!important;"
                                    alt="Курс Фунта" src="http://xn--2-stbsei.xn--p1ai/images/flag2/gbp.png"></a></td>
                        <td style="vertical-align: middle!important;border-top:5px solid #ffffff;border-left: 2px solid #ffffff;padding-left: 3px;padding-right: 3px; color: #ffffff;font-size: 14px;vertical-align: middle!important;"
                            id="USDGBP">00.0000
                        </td>
                        <td class="nothing" style="vertical-align: middle!important;border-top:5px solid #ffffff;border-left: 2px solid #ffffff; text-align: right; padding: 3px 3px;">
                            <img src="" id="GBPsrc"
                                 style="border-right: 2px solid transparent;vertical-align: middle!important;"><span
                                style="color:#ffffff;font-size: 12px;" id="GBPtm">0.000</span></td>
                    </tr>
                    <tr style=" font-family: Tahoma, Geneva, sans-serif; background: linear-gradient(to bottom, #ADACC6, #383C55);">
                        <td style="vertical-align: middle!important;border-top:5px solid #ffffff;padding: 0 3px 0 3px;vertical-align: middle!important;">
                            <a href="http://xn--2-stbsei.xn--p1ai/belarus_ruble-moscow" title="Курс белорусского рубля"
                               target="_blank"
                               style="color: #ffffff; text-decoration: initial;margin-right: 6px; cursor: auto;font-weight: bold;font-size: 14px;">BYR</a>
                        </td>
                        <td style="vertical-align: middle!important;border-top:5px solid #ffffff;border-left: 2px solid #ffffff; text-align: center;">
                            <a href="http://xn--2-stbsei.xn--p1ai/belarus_ruble-moscow" target="_blank"
                               title="Курс белорусского рубля"><img
                                    style="width: 23px; height: 14px; vertical-align: middle!important;"
                                    alt="Курс белорусского рубля"
                                    src="http://xn--2-stbsei.xn--p1ai/images/flag2/byr.png"></a></td>
                        <td style="vertical-align: middle!important;border-top:5px solid #ffffff;border-left: 2px solid #ffffff;padding-left: 3px;padding-right: 3px; color: #ffffff;font-size: 14px;vertical-align: middle!important;"
                            id="USDBYR">00.0000
                        </td>
                        <td class="nothing" style="vertical-align: middle!important;border-top:5px solid #ffffff;border-left: 2px solid #ffffff; text-align: right; padding: 3px 3px;">
                            <img src="" id="BYRsrc"
                                 style="border-right: 2px solid transparent;vertical-align: middle!important;"><span
                                style="color:#ffffff;font-size: 12px;" id="BYRtm">0.000</span></td>
                    </tr>
                    <tr style=" font-family: Tahoma, Geneva, sans-serif; background: linear-gradient(to bottom, #ADACC6, #383C55);">
                        <td style="vertical-align: middle!important;border-top:5px solid #ffffff;padding: 0 3px 0 3px;vertical-align: middle!important;">
                            <a href="http://xn--2-stbsei.xn--p1ai/griven-moscow" title="Курс гривны" target="_blank"
                               style="color: #ffffff; text-decoration: initial;margin-right: 6px; cursor: auto;font-weight: bold;font-size: 14px;">UAH</a>
                        </td>
                        <td style="vertical-align: middle!important;border-top:5px solid #ffffff;border-left: 2px solid #ffffff; text-align: center;">
                            <a href="http://xn--2-stbsei.xn--p1ai/griven-moscow" target="_blank"
                               title="Курс гривны"><img
                                    style="width: 23px; height: 14px; vertical-align: middle!important;"
                                    alt="Курс гривны" src="http://xn--2-stbsei.xn--p1ai/images/flag2/uah.png"></a></td>
                        <td style="vertical-align: middle!important;border-top:5px solid #ffffff;border-left: 2px solid #ffffff;padding-left: 3px;padding-right: 3px; color: #ffffff;font-size: 14px;vertical-align: middle!important;"
                            id="USDUAH">00.0000
                        </td>
                        <td class="nothing" style="vertical-align: middle!important;border-top:5px solid #ffffff;border-left: 2px solid #ffffff; text-align: right; padding: 3px 3px;">
                            <img src="" id="UAHsrc"
                                 style="border-right: 2px solid transparent;vertical-align: middle!important;"><span
                                style="color:#ffffff;font-size: 12px;" id="UAHtm">0.000</span></td>
                    </tr>
                    <tr style=" font-family: Tahoma, Geneva, sans-serif; background: linear-gradient(to bottom, #ADACC6, #383C55);">
                        <td style="vertical-align: middle!important;border-top:5px solid #ffffff;padding: 0 3px 0 3px;vertical-align: middle!important;">
                            <a href="http://xn--2-stbsei.xn--p1ai/griven-moscow" title="Курс гривны" target="_blank"
                               style="color: #ffffff; text-decoration: initial;margin-right: 6px; cursor: auto;font-weight: bold;font-size: 14px;">KZT</a>
                        </td>
                        <td style="vertical-align: middle!important;border-top:5px solid #ffffff;border-left: 2px solid #ffffff; text-align: center;">
                            <a href="http://xn--2-stbsei.xn--p1ai/griven-moscow" target="_blank"
                               title="Курс гривны"><img
                                    style="width: 23px; height: 14px; vertical-align: middle!important;"
                                    alt="Курс гривны" src="https://www.cia.gov/library/publications/the-world-factbook/graphics/flags/large/kz-lgflag.gif"></a></td>
                        <td style="vertical-align: middle!important;border-top:5px solid #ffffff;border-left: 2px solid #ffffff;padding-left: 3px;padding-right: 3px; color: #ffffff;font-size: 14px;vertical-align: middle!important;"
                            id="USDKZT">00.0000
                        </td>
                    </tr>
                    <tr style=" font-family: Tahoma, Geneva, sans-serif; background: linear-gradient(to bottom, #ADACC6, #383C55);">
                        <td style="vertical-align: middle!important;border-top:5px solid #ffffff;padding: 0 3px 0 3px;vertical-align: middle!important;">
                            <a href="http://xn--2-stbsei.xn--p1ai/griven-moscow" title="Курс гривны" target="_blank"
                               style="color: #ffffff; text-decoration: initial;margin-right: 6px; cursor: auto;font-weight: bold;font-size: 14px;">AZN</a>
                        </td>
                        <td style="vertical-align: middle!important;border-top:5px solid #ffffff;border-left: 2px solid #ffffff; text-align: center;">
                            <a href="http://xn--2-stbsei.xn--p1ai/griven-moscow" target="_blank"
                               title="Курс гривны"><img
                                    style="width: 23px; height: 14px; vertical-align: middle!important;"
                                    alt="Курс гривны" src="http://www.worldbank.org/content/dam/Worldbank/Flags/flag_azerbaijan.jpg"></a></td>
                        <td style="vertical-align: middle!important;border-top:5px solid #ffffff;border-left: 2px solid #ffffff;padding-left: 3px;padding-right: 3px; color: #ffffff;font-size: 14px;vertical-align: middle!important;"
                            id="USDAZN">00.0000
                        </td>
                    </tr>
                    <tr style=" font-family: Tahoma, Geneva, sans-serif; background: linear-gradient(to bottom, #ADACC6, #383C55);">
                        <td style="vertical-align: middle!important;border-top:5px solid #ffffff;padding: 0 3px 0 3px;vertical-align: middle!important;">
                            <a href="http://xn--2-stbsei.xn--p1ai/griven-moscow" title="Курс гривны" target="_blank"
                               style="color: #ffffff; text-decoration: initial;margin-right: 6px; cursor: auto;font-weight: bold;font-size: 14px;">UZS</a>
                        </td>
                        <td style="vertical-align: middle!important;border-top:5px solid #ffffff;border-left: 2px solid #ffffff; text-align: center;">
                            <a href="http://xn--2-stbsei.xn--p1ai/griven-moscow" target="_blank"
                               title="Курс гривны"><img
                                    style="width: 23px; height: 14px; vertical-align: middle!important;"
                                    alt="Курс гривны" src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/84/Flag_of_Uzbekistan.svg/125px-Flag_of_Uzbekistan.svg.png"></a></td>
                        <td style="vertical-align: middle!important;border-top:5px solid #ffffff;border-left: 2px solid #ffffff;padding-left: 3px;padding-right: 3px; color: #ffffff;font-size: 14px;vertical-align: middle!important;"
                            id="USDUZS">00.0000
                        </td>
                    </tr>
                    <script src="http://xn--2-stbsei.xn--p1ai/b7/generateCode"></script>
                </table>
                <noscript><strong><a href="http://xn--2-stbsei.xn--j1amh/informer" title="информер курса валют">курс2.укр</a>,
                        Толкование снов онлайн, <a href="http://xn--2-otbgkaeu.xn--p1ai/" target="_blank"
                                                   title="Сонник онлайн">xn--2-otbgkaeu.xn--p1ai</a>, правдивый сонник,
                        bucatarii clasice in Chisinau <a href="http://amevita.md/bucatarii-la-comanda/" target="_blank"
                                                         title="bucatarii clasice">
                            http://amevita.md/bucatarii-la-comanda/</a> Cel mai mare producator de <a
                            href="http://paturi.md/dormitoare-moderne-la-comanda-chisinau.html" target="_blank"
                            title="dormitoare in Chisinau, Moldova">dormitoare md</a> din Moldova Paturi.md, de asemenea
                        puteți comanda <a href="http://paturi.md/dormitoare-mobila-la-comanda-chisinau.html"
                                          target="_blank" title="mobila la comanda">mobila</a> si an credit bucatarii,
                        mobilier</strong></noscript>
            </div><!--  END: topbiz.md Widget HTML 1.0--></p>
            <div id="money-bottom"></div>
        </div>
    </div>
    <div id="adv">
        <a href="https://money.yandex.ru" target="_blank">
            <img src="https://money.yandex.ru/img/yamoney_button.gif"
                 alt="Я принимаю Яндекс.Деньги"
                 title="Я принимаю Яндекс.Деньги" border="0" width="88" height="31"/></a>
        <a href="http://www.wmtransfer.com/" target="_blank">
        <img src="http://www.webmoney.ru/img/icons/wmlogo_vector_blue.png">
            </a>
        <a href="https://qiwi.ru/" target="_blank">
        <img src="/images/ban3.jpg">
        </a>
        <a href="//www.free-kassa.ru/" target="_blank"><img src="//www.free-kassa.ru/img/fk_btn/7.png"></a>
            <a href="http://www.rbkmoney.com/en" target="_blank">
        <img src="/images/ban5.jpg">
            </a>
                <a href="https://www.mastercard.us/en-us.html" target="_blank">
        <img src="/images/ban6.jpg">
                </a>
                    <a href="https://perfectmoney.is/" target="_blank">
        <img src="/images/ban7.png">
                    </a>
    </div>
</div>

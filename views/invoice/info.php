<?php
use yii\helpers\Html;

if($success){
    echo Html::decode('<div class="alert alert-success">'.Yii::t('app','Payment successful').'</div>');
}else{
    echo Html::decode('<div class="alert alert-danger">'.Yii::t('app','Payment failed').'</div>');
}

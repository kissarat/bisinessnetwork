<?php

use app\models\Invoice;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model Invoice */
/* @var $form yii\widgets\ActiveForm */
$statuses = [];
foreach(Invoice::$statuses as $key => $value) {
    $statuses[$key] = Yii::t('app', $value);
}

$buttonText = $model->withdraw ? 'Withdraw' : 'Payment';
if ('choose' == Yii::$app->controller->action->id) {
    $buttonText = 'Buy';
}
?>

<div class="invoice-form">

    <?php $form = ActiveForm::begin();

    if ('manage' == $model->scenario) {
        echo $form->field($model, 'user_id')->textInput(['maxlength' => true]);
        echo $form->field($model, 'status')->dropDownList($statuses);
    }
    echo Html::activeHiddenInput($model, 'withdraw');
    echo $form->field($model, 'amount')->textInput([
        'readonly' => 'choose' == Yii::$app->controller->action->id
    ]);
    ?>

    <p>
        <?= Html::submitButton(Yii::t('app', $buttonText),['class'=>'btn btn-primary']) ?>
    </p>

    <?php ActiveForm::end(); ?>
</div>

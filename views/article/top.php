<?php
use app\widgets\Panel;
use yii\helpers\Html;

?>
<div class="article-top">
    <div id="left-panel">
    <?= Yii::$app->view->render('_categories', ['categories' => $categories]) ?>
    <?php
    $shopping = Yii::t('app','Items').": 0<br>".Yii::t('app','Price').": 0";
    echo Panel::widget([
        'options' => ['class' => 'shopping col-lg-2'],
        'model'=>[
            'title'=>Yii::t('app','Shopping'),
            'content'=>$shopping
        ]
    ]);
    ?>
        <div id="adv">
            <a href="https://money.yandex.ru" target="_blank">
                <img src="https://money.yandex.ru/img/yamoney_button.gif"
                     alt="Я принимаю Яндекс.Деньги"
                     title="Я принимаю Яндекс.Деньги" border="0" width="88" height="31"/></a><br>
        <img src="http://www.webmoney.ru/img/icons/wmlogo_vector_blue.png"><br>
        <img src="/images/ban3.jpg"><br>
            <a href="//www.free-kassa.ru/"><img src="//www.free-kassa.ru/img/fk_btn/7.png"></a>
        <img src="/images/ban5.jpg"><br>
        <img src="/images/ban6.jpg"><br>
            <img src="/images/ban7.png">
        </div>
    </div>
    <div class="lists col-lg-10">
    <?php

    foreach ($lists as $title => $list) {
        $items = [];
        if($title==Yii::t('app','Novelty')){
            $one = [];
            $two = [];
            $three = [];
            foreach($list as $item){
                if($item->parent_id == '1points'){
                    array_push($one,$item);
                } else if($item->parent_id == '2points'){
                    array_push($two,$item);
                }else if($item->parent_id == '3points'){
                    array_push($three,$item);
                }
            }
            $image1=[];
            foreach($one as $item){
                if (file_exists('data/article/image/' . $item['id'] . '.jpg')) {
                    array_push($image1, "<img src='/data/article/image/" . $item['id'] . ".jpg'>");
                }
            }
            $image2=[];
            foreach($two as $item){
                if (file_exists('data/article/image/' . $item['id'] . '.jpg')) {
                    array_push($image2, "<img src='/data/article/image/" . $item['id'] . ".jpg'>");
                }
            }
            $image3=[];
            foreach($three as $item){
                if (file_exists('data/article/image/' . $item['id'] . '.jpg')) {
                    array_push($image3, "<img src='/data/article/image/" . $item['id'] . ".jpg'>");
                }
            }

            $content = Yii::$app->view->render('_novely',[
                'image1'=>$image1,
                'image2'=>$image2,
                'image3'=>$image3
            ]);
            echo $content;
        }else{
            foreach ($list as $item) {
                $items[] = Yii::$app->view->render('_product', ['model' => $item]);
            }
            echo Panel::widget([
                'options' => ['class' => 'lists col-lg-10'],
                'model' => [
                    'title' => $title,
                    'content' => Html::ul($items, ['encode' => false])
                ]
            ]);
        }
    }
    ?>
    </div>
</div>

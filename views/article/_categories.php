<?php
use app\widgets\Panel;
use yii\helpers\Html;

if (!empty($categories)) {
    echo Panel::widget([
        'options' => ['class' => 'menu categories col-lg-2'],
        'model' => [
            'title' => Yii::t('app', 'Categories'),
            'content' => implode("\n", array_map(function($category) {
                return Html::a($category->title, ['index', 'root_id' => $category->id, 'lang' => $category->lang]);
            }, $categories))
        ]
    ]);
}

?>

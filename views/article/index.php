<?php
use app\models\Article;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ListView;

/** @var \yii\data\DataProviderInterface $dataProvider */
/** @var int $parent_id */

$params = [
    'dataProvider' => $dataProvider,
]

?>
<div class="article-index <?= !empty($categories) ? 'row' : '' ?>">
    <?= Yii::$app->view->render('_categories', ['categories' => $categories]) ?>
    <div class="<?= $layout ?>-layout <?= !empty($categories) ? 'col-lg-10' : '' ?>">
        <?php
        if (isset($_GET['root_id'])) {
            $model = Article::findOne($_GET['root_id']);
            $categories = $model->categories;
            if (count($categories) > 1) {
                foreach ($model->categories as $parent) {
                    $this->params['breadcrumbs'][] = [
                        'label' => $parent->title,
                        'url' => ['/article/index', 'root_id' => $parent->id, 'lang' => $parent->lang]
                    ];
                }
                require 'view.php';
            }
        }

        if ('grid' == $layout):
            echo GridView::widget(array_merge($params, [
                'columns' => [
                    'id',
                    [
                        'attribute' => 'title',
                        'format' => 'html',
                        'value' => function ($model) {
                            return Html::a($model->title, ['view', 'id' => $model->id]);
                        }
                    ],
                    [
                        'attribute' => 'summary',
                        'format' => 'html',
                        'value' => function ($model) {
                            return strip_tags($model->summary);
                        }
                    ],
                    [
                        'format' => 'html',
                        'value' => function ($model) {
                            return Html::a('', ['/article/edit', 'id' => $model->id], [
                                'class' => 'glyphicon glyphicon-pencil',
                                'title' => Yii::t('app', 'Edit')
                            ]);
                        }
                    ]
                ]
            ]));
        else:
            echo ListView::widget(array_merge($params, [
                'summary' => false,
                'itemView' => '_post'
            ]));
        endif;
        ?>
    </div>
</div>

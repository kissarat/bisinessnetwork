<?php
/** @var \app\models\Article $model */
/** @var \yii\web\View $this */

use yii\helpers\Html;


if ('view' == Yii::$app->controller->action->id) {
    foreach ($model->categories as $category) {
        $this->params['breadcrumbs'][] = [
            'label' => $category->title,
            'url' => ['/article/index', 'root_id' => $category->id, 'lang' => $model->lang]
        ];
    }
}

if (empty($this->metaTags)) {
    foreach ($model->getMetaTags() as $name => $content) {
        $this->registerMetaTag([
            'name' => $name,
            'content' => $content
        ]);
    }
}
$img = "";
if(file_exists('data/article/image/' . $model->safeId() . '.jpg')) {
    $img = Html::tag('figure', Html::img('/data/article/image/' . $model->safeId() . '.jpg'));
}
?>
<div class="article-view">
    <?php if (!$model->getIsNewRecord()): ?>
        <?php if ($model->price) { ?>
            <div class="head">
                <div class="title">
                    <?php
                    $price = null;
                    if(!Yii::$app->user->getIsGuest()){
                        $price = $model->price."$";
                    } ?>
                    <?= Html::tag('h1', $model->title) ?>
                    <?= Html::a(Yii::t('app', 'Order')." ".$price,
                        ['/order/edit', 'product_id' => $model->id, 'user_id' => Yii::$app->user->id],
                        ['class' => 'button']) ?>
                </div>
                <?= $img ?>
            </div>
        <?php } else { ?>
            <?= Html::tag('h1', $model->title) ?>
            <?= $img ?>
        <?php } ?>
            <div class="article-content">
                <?= $model->content ?>
            </div>
        <?php elseif (Yii::$app->user->can('manage')): ?>
        <?= Html::a(Yii::t('app', 'Edit text'), ['/article/edit']) ?>
    <?php endif ?>
</div>

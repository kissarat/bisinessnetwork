<?php
use app\models\Article;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Html;

/** @var Article $model */

$this->title = Yii::t('app', 'Update');
//\app\helpers\EditorAsset::register($this);
?>
<div class="article-edit">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'id') ?>
    <div class="form-group field-language">
        <label><?= Yii::t('app', 'Language') ?>:</label>
        <?php
        $languages = [];
        foreach(Article::languages() as $code => $name) {
            $languages[] = $model->lang == $code ? $name : Html::a($name, ['edit', 'id' => $model->id, 'lang' => $code]);
        }
        echo implode(' ', $languages);
        ?>
    </div>
    <?= $form->field($model, 'parent_id') ?>
    <?= Html::tag('div', $model->parent_id ? $model->parent->title : '', ['id' => 'category_title']) ?>
    <?= $form->field($model, 'title') ?>
    <?= $form->field($model, 'keywords') ?>
    <?= $form->field($model, 'price') ?>
    <?= $form->field($model, 'image')->fileInput(['accept' => 'image/*']) ?>
    <?= $form->field($model, 'summary')->textarea() ?>
    <?= $form->field($model, 'content')->widget(CKEditor::className(), [
        'preset' => 'full'
    ]) ?>

    <?= Html::submitButton($this->title) ?>

    <?php ActiveForm::end(); ?>

    <script>
        addEventListener('load', function() {
            document.head.appendChild($new('script', {src:'/youtube.js'}));
        });
    </script>
</div>

<?php
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<a class="article-product" href="<?= $url ?>">
    <?= Html::tag('h3', $model->title) ?>
    <?php if(file_exists('data/article/image/' . $model->id . '.jpg')) {
       // echo Html::tag('img', '/data/article/image/' . $model->id . '.jpg');
        echo "<img src='/data/article/image/" . $model->id . ".jpg'>";
    }
    ?>
</a>

<?php
/**
 * Created by PhpStorm.
 * User: yura
 * Date: 20.02.16
 * Time: 20:03
 */
use app\widgets\Panel;
foreach($model as $item) {
    echo Panel::widget([
        'options' => ['class' => 'col-lg-4'],
        'model' => [
            'title' => $item->title,
            'content' => $item->content
        ]
    ]);
}
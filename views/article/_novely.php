<?php
/**
 * Created by PhpStorm.
 * User: yura
 * Date: 21.02.16
 * Time: 1:05
 */
use app\widgets\Panel;
use yii\bootstrap\Carousel;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\Url;

//$url = Url::to(['view', 'id' => $one->id, 'lang' => Yii::$app->language]);
//$query = new Query();
//$query->select('title')->from('article')->where(['id'=>$one->parent_id]);
//$category = $query->one();
$content = Carousel::widget([
    'items' => $image1,
    'options'=>[
        'id'=>'carousel-example-generic-1',
        'data-ride'=>'carousel',
        'class'=>'slide'
    ],
    'controls'=>[Html::decode('<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>'), Html::decode('<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>')]
]);

echo Panel::widget([
    'options' => ['class' => 'new'],
    'model' => [
        'title' => Yii::t('app','1 point product'),
        'content' => $content
    ]
]);
$content = Carousel::widget([
    'items' => $image2,
    'options'=>[
        'id'=>'carousel-example-generic-2',
        'data-ride'=>'carousel',
        'class'=>'slide'
    ],
    'controls'=>[Html::decode('<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>'), Html::decode('<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>')]
]);

echo Panel::widget([
    'options' => ['class' => 'new'],
    'model' => [
        'title' => Yii::t('app','2 point product'),
        'content' => $content
    ]
]);

$content = Carousel::widget([
    'items' => $image3,
    'options'=>[
        'id'=>'carousel-example-generic-3',
        'data-ride'=>'carousel',
        'class'=>'slide'
    ],
    'controls'=>[Html::decode('<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>'), Html::decode('<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>')]
]);

echo Panel::widget([
    'options' => ['class' => 'new'],
    'model' => [
        'title' => Yii::t('app','3 point product'),
        'content' => $content
    ]
]);
?>
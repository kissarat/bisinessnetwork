<?php
use yii\helpers\Html;
?>
<div class="article-post">
    <?php if(file_exists('data/article/thumb/' . $model->safeId() . '.jpg')){ ?>
    <figure>
        <?= Html::a( Html::img('/data/article/thumb/' . $model->safeId() . '.jpg'), ['view', 'id' => $model->id, 'lang' => $model->lang]) ?>

    </figure>
    <?php } ?>
        <?= Html::a($model->title, ['view', 'id' => $model->id, 'lang' => $model->lang]) ?>
        <br>
        <?php
        if(!Yii::$app->user->getIsGuest() && $model->price!=null) {
           echo Html::a(Yii::t('app', 'Order') . " " . $price,
                ['/order/edit', 'product_id' => $model->id, 'user_id' => Yii::$app->user->id],
                ['class' => 'button']);
            }
        ?>

    <div>
        <?php
        if (Yii::$app->user->can('manage')) {
            echo Html::a(Yii::t('app', 'Edit'), ['/article/edit', 'id' => $model->id]);
        }
        ?>
    </div>
</div>

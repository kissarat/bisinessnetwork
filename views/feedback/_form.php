<?php
/**
 * @link http://zenothing.com/
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="feedback-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="contact">
        <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'subject') ?>
    </div>
    <?= $form->field($model, 'content')->textarea() ?>

    <p class="buttons">
        <?= Html::submitButton(Html::img('/images/send.jpg')) ?>
    </p>

    <?php ActiveForm::end(); ?>

</div>

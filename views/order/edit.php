<?php
use app\widgets\Fake;
use yii\bootstrap\Html;
use yii\db\Query;
use yii\widgets\ActiveForm;
?>
<div class="order-edit">
    <p>
        <?php
        if (Yii::$app->user->can('manage')) {
            echo Fake::widget();
        }
        $query = new Query();
        $query->select('email,forename,surname,patronymic,phone,skype,country,postal,city,address')->from('user')->where(['id'=>Yii::$app->user->id]);
        $user = $query->one();
        $model->email = $user['email'];
        $model->forename = $user['forename'];
        $model->surname = $user['surname'];
        $model->patronymic = $user['patronymic'];
        $model->phone = $user['phone'];
        $model->skype = $user['skype'];
        $model->country = $user['country'];
        $model->postal = $user['postal'];
        $model->city = $user['city'];
        $model->address = $user['address'];
        ?>
    </p>
    <?php $form = ActiveForm::begin([
        'options' => ['name' => 'Order']
    ]); ?>

    <?= $form->field($model, 'email') ?>
    <?= $form->field($model, 'forename') ?>
    <?= $form->field($model, 'surname') ?>
    <?= $form->field($model, 'patronymic') ?>
    <?= $form->field($model, 'phone') ?>
    <?= $form->field($model, 'skype') ?>
    <?= $form->field($model, 'country') ?>
    <?= $form->field($model, 'postal') ?>
    <?= $form->field($model, 'city') ?>
    <?= $form->field($model, 'address') ?>
    <?= Html::submitButton(Yii::t('app', $model->getIsNewRecord() ? 'Create' : 'Save')) ?>
    <? ActiveForm::end() ?>
</div>

<?php
/** @var \yii\data\ActiveDataProvider $dataProvider */
use app\models\Invoice;
use app\models\Order;
use yii\grid\GridView;
use yii\helpers\Html;

$columns = [
    'id',
//    [
//        'attribute' => 'invoice_id',
//        'format' => 'html',
//        'value' => function(Order $model) {
//            return Html::a(Yii::t('app', Invoice::$statuses[$model->status]), ['/invoice/view', 'id' => $model->invoice_id]);
//        }
//    ],
    [
        'attribute' => 'product_id',
        'format' => 'html',
        'value' => function(Order $model) {
            return Html::a($model->title, ['/article/view', 'id' => $model->id]);
        }
    ],
    'price:integer'
];

if (empty($user_id)) {
    $columns[] = [
        'attribute' => 'user_id',
        'format' => 'html',
        'value' => function(Order $model) {
            return Html::a($model->user_id, ['/user/view', 'user_id' => $model->user_id]);
        }
    ];
}
elseif(Yii::$app->user->can('manage')) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orders'),'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => $user_id];
}

?>
<div class="order-index">
    <?php
    if (Yii::$app->user->can('manage') && isset($user_id)) {
        echo Html::a($user_id, ['/user/view', 'id' => $user_id]);
    }
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns
    ]) ?>
</div>

<?php
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\User;
use yii\widgets\ActiveForm;

/**
 * Created by PhpStorm.
 * User: yura
 * Date: 16.02.16
 * Time: 18:06
 */
?>
<div class="user-edit">
    <?php if(!$create) { ?>
    <label class="control-label"><?= Yii::t('app', 'Link') ?> <a href="<?= Url::to(['capture/capturepage', 'user_id' => $model->user_id], true) ?>" target="_blank"><span
            class="label label-warning"><?= Url::to(['capture/capturepage', 'user_id' => $model->user_id], true) ?></span></a>
    </label>
    <?php
     }
//    if($succ){
//        echo "<div class=\"alert alert-success\" role=\"alert\">".Yii::t('app','Successful')."</div>";
//    }
$form = ActiveForm::begin(['id'=>'capture-form']); ?>
<?= $form->field($model, 'content')->widget(CKEditor::className(), [
    'preset' => 'full'
]) ?>
    <p class="buttons">
        <?= Html::submitButton($create ? Yii::t('app','Create'): Yii::t('app','Update'), ['class' => 'btn btn-primary']) ?>
    </p>

<?php ActiveForm::end(); ?>
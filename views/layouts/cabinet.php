<?php
use app\widgets\Menu;
use app\widgets\Panel;
use yii\db\Query;
use yii\helpers\Html;

$this->beginContent('@app/views/layouts/main.php');

$user = Yii::$app->user;
$query = new Query();
$query->select('id')->from('matrix.node')->where(['user_id'=>$user->id])->orderBy('type_id DESC');
$type_id = $query->one();
$binar = null;
if($type_id['id']) {
   $binar =  Html::a(Yii::t('app', 'Binar'), ['/matrix/node/graph', 'id' => $type_id['id']]);
    }
$items = [
    [
        'match' => '/^user/',
        'header' => Yii::t('app', 'Cabinet'),
        'content' => [
            Yii::t('app', 'View') => ['/user/view', 'id' => $user->id],
            Yii::t('app', 'Update') => ['/user/update', 'id' => $user->id],
            Yii::t('app', 'Wallets') => ['/user/wallet', 'id' => $user->id],
            Yii::t('app', 'Password') => ['/user/password', 'id' => $user->id],
            Yii::t('app', 'Logout') => ['/user/logout', 'id' => $user->id],
        ]
    ],
    Html::a(Yii::t('app', 'Team'), ['/user/index', 'referral_id' => $user->id]),
    $binar,
        [
        'match' => '/^transfer/',
        'header' => Yii::t('app', 'Transfers'),
        'content' => [
            Yii::t('app', 'Sent') => ['/transfer/index',
                'type' => 'user', 'event' => 'transfer', 'user_id' => $user->id],
            Yii::t('app', 'Received') => ['/transfer/index',
                'type' => 'user', 'event' => 'transfer', 'object_id' => $user->id],
            Yii::t('app', 'Transfer') => ['/transfer/create', 'user_id' => $user->id],
        ]
    ],
    [
        'match' => '/^matrix/',
        'header' => Yii::t('app', 'Programs'),
        'content' => [
            Yii::t('app', 'Opened') => ['/matrix/node/index', 'user_id' => $user->id],
            Yii::t('app', 'Buy') => ['/matrix/node/create', 'user_id' => $user->id],
        ]
    ],
    [
        'match' => '/^invoice/',
        'header' => Yii::t('app', 'Invoices'),
        'content' => [
            Yii::t('app', 'Payment') => ['/invoice/payment', 'user_id' => $user->id],
            Yii::t('app', 'History') => ['/invoice/index', 'user_id' => $user->id],
            Yii::t('app', 'Withdraw') => ['/invoice/withdraw', 'user_id' => $user->id],
        ]
    ],
    Html::a(Yii::t('app', 'Orders'), ['/order/index', 'user_id' => $user->id]),
    Html::a(Yii::t('app', 'Capture page'), ['/capture/captureedit', 'user_id' => $user->id]),
];
?>
<div class="row" id="cabinet">
    <div class="col-sm-2" id="menu">
        <?= Panel::widget([
            'options' => ['class' => 'collapsible menu'],
            'model' => [
                'title' => Yii::t('app', 'Categories'),
                'content' => Menu::widget(['items' => $items])
            ]
        ]) ?>
    </div>
    <div class="col-sm-10" id="content">
        <?= $content ?>
    </div>
</div>
<?php
$this->endContent();

<?php
use app\helpers\MainAsset;
use app\models\Article;
use app\models\Config;
use app\models\LoginForm;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

MainAsset::register($this);
$login = Yii::$app->user->getIsGuest() ? '' : 'login';
$route = [Yii::$app->controller->id, Yii::$app->controller->action->id];

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title><?= Html::encode($this->title) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script src="/moment.js"></script>
    <script type="text/javascript">stLight.options({publisher: "75339d0b-7b4f-46fb-848f-a58570f4dddb", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script> <?php $this->head() ?>
    <?= Html::csrfMetaTags() ?>
</head>
<body class="main-layout <?= implode(' ', $route) ?>">
<div id="bg">
<?php $this->beginBody();
if (!Yii::$app->user->getIsGuest()) {
    $user = Yii::$app->user->id;
    echo Html::script("var user = '$user'");
}
?>
    <div id="bg-shadow-top"></div>
    <div id="bg-shadow-bottom"></div>
<div class="wrap <?= $login ?>">
    <header>
        <div id="top-date"></div>
        <div class="container">
            <div id="logo1"><img src="/images/logo1.png"><span> BISINESSNETWORK1x1.com</span></div>
            <div class="right">
                <?php
                if (Yii::$app->user->getIsGuest()):
                    $login = new LoginForm();
                    ActiveForm::begin([
                        'action' => '/user/login',
                        'id' => 'login'
                    ]);
                    ?>
                    <?= Html::dropDownList('language', Yii::$app->language, [
                    'ru' => 'Русский',
                    'en' => 'English',
                ]);
                    ?>
                    <?= Html::activeTextInput($login, 'name', [
                    'title' => Yii::t('app', 'login')
                ])
                    ?>
                    <?= Html::activePasswordInput($login, 'password', [
                    'title' => Yii::t('app', 'password')
                ])
                    ?>
                    <input name="back" type="hidden"/>
                    <?= Html::submitButton(Html::img('/images/login-button.jpg')) ?>
                    <?php ActiveForm::end(); ?>
                    <div>
                        <?= Html::a(Yii::t('app', 'Forgot your password?'), ['/user/request']) ?>
                        <?= Html::a(Yii::t('app', 'Sign Up'), ['/user/create']) ?>
                    </div>
                    <?php
                else:
                    $cabinet_url = ['/user/view', Yii::$app->user->id];
                    ?>
                    <div>
                    <span class="welcome">
                        <?= Yii::t('app', 'Welcome') ?>,
                    </span>
                        <?= Html::a(Yii::$app->user->id, $cabinet_url, ['class' => 'nickname']) ?>
                        <?= Html::a(Html::img('/images/logout.jpg'), ['/user/logout']); ?>
                    </div>
                    <div class="office">
                        <?= Html::a(Yii::t('app', 'Virtual office'), $cabinet_url) ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </header>

    <?php
    NavBar::begin();

    $items = [
        ['label' => Yii::t('app', 'Home'), 'url' => ['/home/index']],
        ['label' => Yii::t('app', 'About'), 'url' => ['/article/view', 'id' => 'about']],
        ['label' => Yii::t('app', 'Marketing'), 'url' => ['/article/view', 'id' => 'marketing']],
        ['label' => Yii::t('app', 'Shop'), 'url' => ['/article/top', 'root_id' => 'shop', 'lang' => Yii::$app->language]],
        ['label' => Yii::t('app', 'Gallery'), 'url' => ['/article/gallery']],
        ['label' => Yii::t('app', 'News'), 'url' => ['/article/index', 'root_id' => 'news', 'lang' => Yii::$app->language]],
        ['label' => Yii::t('app', 'School'), 'url' => ['/article/view', 'id' => 'faq']],
        ['label' => Yii::t('app', 'Reviews'), 'url' => ['/review/review/index']],
         ['label' => Yii::t('app', 'Contacts'), 'url' => ['/home/contact']],
    ];

    if (Yii::$app->user->can('manage')) {
        $items[] = ['label' => Yii::t('app', 'Generate'), 'url' => ['/test/generate/index']];
    }

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => $items
    ]);
    NavBar::end();
    ?>
    <div class="container">
        <?= Breadcrumbs::widget([
            'homeLink' => false,
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>

        <?= Alert::widget() ?>

        <?php
        if ('cabinet' != Yii::$app->layout && 'article' != Yii::$app->controller->id && Yii::$app->user->can('manage')) {
            echo Article::widget(Yii::$app->request->getPathInfo());
        }
        ?>

        <?= $content ?>
        <div id="social">
            <span class='st_facebook_large' displayText='Facebook'></span>
            <span class='st_googleplus_large' displayText='Google +'></span>
            <span class='st_livejournal_large' displayText='LiveJournal'></span>
            <span class='st_odnoklassniki_large' displayText='Odnoklassniki'></span>
            <span class='st_twitter_large' displayText='Tweet'></span>
            <span class='st_vkontakte_large' displayText='Vkontakte'></span>
            <span class='st_instagram_large' displayText='Instagram Badge'></span>
        </div>
    </div>

    <footer>
        &copy <?= date('Y') . ' ' . Yii::t('app', 'All rights reserved') ?>

    </footer>
</div>

<?php $this->endBody() ?>

<?php if (Config::get('common', 'heart')): ?>
    <script>
        (function () {
            var widget_id = <?= Config::get('common', 'heart') ?>;
            _shcp = [{widget_id: widget_id}];
            var lang = (navigator.language || navigator.systemLanguage
            || navigator.userLanguage || "en")
                .substr(0, 2).toLowerCase();
            var url = "widget.siteheart.com/widget/sh/" + widget_id + "/" + lang + "/widget.js";
            var hcc = document.createElement("script");
            hcc.type = "text/javascript";
            hcc.async = true;
            hcc.src = ("https:" == document.location.protocol ? "https" : "http")
                + "://" + url;
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hcc, s.nextSibling);
        })();
    </script>
<?php endif ?>

<?php if (Config::get('common', 'google')): ?>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', '<?= Config::get('common', 'google') ?>', 'auto');
        ga('require', 'linkid', 'linkid.js');
        ga('send', 'pageview');
    </script>
<?php endif ?>
</div>
</body>
</html>
<?php $this->endPage() ?>

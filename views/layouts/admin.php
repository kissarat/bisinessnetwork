<?php
use app\helpers\MainAsset;
use app\models\Article;
use app\widgets\Alert;
use app\widgets\Menu;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

MainAsset::register($this);
$login = Yii::$app->user->getIsGuest() ? '' : 'login';
$route = [Yii::$app->controller->id, Yii::$app->controller->action->id];

$items = !Yii::$app->user->can('manage') ? [
    Yii::t('app', 'Login') => ['/user/login']
]
    : [
        Yii::t('app', 'Logout') => ['/user/logout'],
        [
            'match' => '/^user/',
            'header' => Yii::t('app', 'Users'),
            'content' => [
                Yii::t('app', 'List') => ['/user/index'],
                Yii::t('app', 'Create') => ['/admin/user/create'],
                Yii::t('app', 'Profile') => ['/user/view', 'id' => Yii::$app->user->id],
            ],
        ],
        [
            'match' => '/^invoice/',
            'header' => Yii::t('app', 'Invoices'),
            'content' => [
                Yii::t('app', 'History') => ['/invoice/index'],
            ],
        ],
        Html::a(Yii::t('app', 'Transfers'), ['/transfer/index',
            'type' => 'user', 'event' => 'transfer']),
        [
            'match' => '/^matrix/',
            'header' => Yii::t('app', 'Programs'),
            'content' => [
                Yii::t('app', 'List') => ['/matrix/node/index'],
            ],
        ],
        [
            'match' => '/^article/',
            'header' => Yii::t('app', 'Pages'),
            'content' => [
                Yii::t('app', 'News') => ['/article/index', 'root_id' => 'news'],
                Yii::t('app', 'Shop') => ['/article/index', 'root_id' => 'shop'],
                Yii::t('app', 'About') => ['/article/view','id'=>'about'],
                Yii::t('app', 'Marketing') => ['/article/view','id'=>'marketing'],
                Yii::t('app', 'Gallery') => ['/article/index','root_id'=>'gallery'],
                Yii::t('app', 'School') => ['/article/view','id'=>'faq'],
                Yii::t('app', 'Create') => ['/article/edit'],
                Yii::t('app', 'Edit') => ['/home/editpages'],
            ],
        ],
        [
            'match' => '/^review/',
            'header' => Yii::t('app', 'Reviews'),
            'content' => [
                Yii::t('app', 'List') => ['/review/review/index'],
                Yii::t('app', 'Create') => ['/review/review/create'],
            ],
        ],
    ];

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title><?= Html::encode($this->title) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
    <?php $this->head() ?>
    <?= Html::csrfMetaTags() ?>
</head>
<body class="<?= implode(' ', $route) ?>">
<?php $this->beginBody();
if (!Yii::$app->user->getIsGuest()) {
    $user = Yii::$app->user->id;
    echo Html::script("var user = '$user'");
}
?>
<div id="admin">
    <div class="col-sm-2" id="menu">
        <?= Html::tag('h2', Yii::t('app', 'Admin Panel')) ?>
        <?= Menu::widget([
            'items' => $items
        ])?>
    </div>
    <div class="col-sm-10" id="content">
        <?= Breadcrumbs::widget([
            'homeLink' => false,
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>

        <?= Alert::widget() ?>

        <?php
        if ('cabinet' != Yii::$app->layout && 'article' != Yii::$app->controller->id && Yii::$app->user->can('manage')) {
            echo Article::widget(Yii::$app->request->getPathInfo());
        }
        ?>

        <?= $content ?>
    </div>
</div>

<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>

<?php
/**
 * Created by PhpStorm.
 * User: yura
 * Date: 16.02.16
 * Time: 9:00
 */
use app\helpers\MainAsset;
use app\models\Article;
use app\models\Config;
use app\models\LoginForm;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

MainAsset::register($this);
$login = Yii::$app->user->getIsGuest() ? '' : 'login';
$route = [Yii::$app->controller->id, Yii::$app->controller->action->id];

?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <title><?= Html::encode($this->title) ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
        <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
        <script src="/moment.js"></script>
        <script type="text/javascript">stLight.options({publisher: "75339d0b-7b4f-46fb-848f-a58570f4dddb", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script> <?php $this->head() ?>
        <?= Html::csrfMetaTags() ?>
    </head>
    <body class="capture-layout <?= implode(' ', $route) ?>">
    <div id="bg">
        <?php $this->beginBody();?>
        <div id="bg-shadow-top"></div>
        <div id="bg-shadow-bottom"></div>
        <div class="wrap <?= $login ?>">
            <header>
                <div class="container">
                    <div id="logo1"><a href="<?= Url::to('/',true)?>" target="_blank"><img src="/images/logo1.png"><br><span> BISINESSNETWORK1x1.com</span></a></div>
                </div>
            </header>

            <div class="container">

                <?= $content ?>
                <div id="social">
                    <span class='st_facebook_large' displayText='Facebook'></span>
                    <span class='st_googleplus_large' displayText='Google +'></span>
                    <span class='st_livejournal_large' displayText='LiveJournal'></span>
                    <span class='st_odnoklassniki_large' displayText='Odnoklassniki'></span>
                    <span class='st_twitter_large' displayText='Tweet'></span>
                    <span class='st_vkontakte_large' displayText='Vkontakte'></span>
                    <span class='st_instagram_large' displayText='Instagram Badge'></span>
                </div>
            </div>


        </div>

        <?php $this->endBody() ?>

        <?php if (Config::get('common', 'heart')): ?>
            <script>
                (function () {
                    var widget_id = <?= Config::get('common', 'heart') ?>;
                    _shcp = [{widget_id: widget_id}];
                    var lang = (navigator.language || navigator.systemLanguage
                    || navigator.userLanguage || "en")
                        .substr(0, 2).toLowerCase();
                    var url = "widget.siteheart.com/widget/sh/" + widget_id + "/" + lang + "/widget.js";
                    var hcc = document.createElement("script");
                    hcc.type = "text/javascript";
                    hcc.async = true;
                    hcc.src = ("https:" == document.location.protocol ? "https" : "http")
                        + "://" + url;
                    var s = document.getElementsByTagName("script")[0];
                    s.parentNode.insertBefore(hcc, s.nextSibling);
                })();
            </script>
        <?php endif ?>

        <?php if (Config::get('common', 'google')): ?>
            <script>
                (function (i, s, o, g, r, a, m) {
                    i['GoogleAnalyticsObject'] = r;
                    i[r] = i[r] || function () {
                            (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date();
                    a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                    a.async = 1;
                    a.src = g;
                    m.parentNode.insertBefore(a, m)
                })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                ga('create', '<?= Config::get('common', 'google') ?>', 'auto');
                ga('require', 'linkid', 'linkid.js');
                ga('send', 'pageview');
            </script>
        <?php endif ?>
    </div>
    </body>
    </html>
<?php $this->endPage() ?>
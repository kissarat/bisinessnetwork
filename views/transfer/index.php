<?php
use yii\grid\GridView;
use yii\helpers\Html;

/** @var \yii\data\ActiveDataProvider $dataProvider */

$url = ['/matrix/node/index'];
if (!Yii::$app->user->can('manage')) {
    $url['user_id'] = Yii::$app->user->id;
}
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Programs'), 'url' => $url];

$columns = [
    'amount:integer',
    'time:datetime',
];

if (empty($_GET['user_id'])) {
    array_unshift($columns,
        [
            'attribute' => 'user_id',
            'label' => Yii::t('app', 'Sender'),
            'format' => 'html',
            'value' => function($model) {
                return Yii::$app->user->can('manage')
                    ? Html::a($model->user_id, ['/user/view', 'id' => $model->user_id])
                    : $model->user_id;
            }
        ]);
}

if (empty($_GET['object_id'])) {
    array_unshift($columns, [
        'attribute' => 'object_id',
        'label' => Yii::t('app', 'Receiver'),
        'format' => 'html',
        'value' => function($model) {
            return Yii::$app->user->can('manage')
                ? Html::a($model->object_id, ['/user/view', 'id' => $model->object_id])
                : $model->object_id;
        }
    ]);
}

?>
<div class="transfer-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns
    ]) ?>
</div>

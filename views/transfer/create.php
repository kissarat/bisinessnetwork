<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="transfer-create">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'receiver_id'); ?>
    <?= $form->field($model, 'amount'); ?>
    <?= Html::submitButton(Yii::t('app', 'Send'), ['class' => 'button']); ?>
    <?php ActiveForm::end(); ?>
</div>

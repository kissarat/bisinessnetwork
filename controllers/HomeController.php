<?php

namespace app\controllers;


use app\models\Article;
use app\models\Config;
use app\models\Feedback;
use Faker\Provider\en_US\Text;
use Yii;
use yii\db\Query;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class HomeController extends Controller {

    public function actionIndex() {
        $query = new Query();
        $query->select('title,id')->from('article')->where(['price'=>null,'parent_id'=>'news'])->orderBy(['id'=>'SORT_DESC'])->limit(5);
        $news = $query->all();
        return $this->render('index',[
            'news'=>$news
        ]);
    }
    public function actionEditpages(){
        if(!Yii::$app->user->can('manage')){
            throw new ForbiddenHttpException;
        }
        return $this->render('editpages');
    }
    public function actionInfo() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'id' => Yii::$app->id,
            'language' => Yii::$app->language,
            'modules' => array_keys(Yii::$app->modules),
            'bootstrap' => Yii::$app->bootstrap,
            'version' => Yii::$app->version,
        ];
    }

    public function actionError() {
        return Yii::$app->getErrorHandler()->exception->getMessage();
    }

    public function actionLanguage($code) {
        setcookie('lang', $code, time() + 3600 * 24 * 30, '/');
        return $this->goBack();
    }

    public function actionSitemap() {
        $sitemap = Yii::$app->cache->get('sitemap');
        if (!$sitemap) {
            $lines = [
                '<?xml version="1.0" encoding="UTF-8"?>',
                '<urlset>'
            ];
            foreach (Article::find()->column() as $id) {
                $lines[] = '<url><loc>' . Url::to(['/article/view', 'id' => $id, 'lang' => 'ru'], true) . '</loc></url>';
            }
            $lines[] = '</urlset>';
            $sitemap = implode("\n", $lines);
            Yii::$app->cache->set('sitemap', $sitemap, 600);
        }
        Yii::$app->response->format = Response::FORMAT_RAW;
        Yii::$app->response->getHeaders()->set('Content-Type', 'text/xml');
        return $sitemap;
    }

    public function actionContact() {
        $model = new Feedback(['scenario' => Yii::$app->user->getIsGuest() ? 'guest' : 'default']);
        return $this->render('contact', [
            'model' => $model
        ]);
    }
}

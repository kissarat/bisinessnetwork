<?php

namespace app\controllers;


use app\behaviors\NoTokenValidation;
use app\models\Invoice;
use League\Flysystem\NotSupportedException;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class InvoiceController extends Controller {

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['payment', 'withdraw', 'success', 'update'],
                'rules' => [
                    [
                        'actions' => ['payment', 'withdraw', 'success'],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                    [
                        'actions' => ['update'],
                        'allow' => true,
                        'roles' => ['admin']
                    ]
                ]
            ],

            'no_csrf' => [
                'class' => NoTokenValidation::className(),
                'only' => ['success', 'fail'],
            ]
        ];
    }

    public function actionIndex($user_id = null) {
        $query = Invoice::find();
        if ('invoice' != $this->id) {
            $query->andWhere(['type' => $this->id]);
        }
        if ($user_id) {
            Yii::$app->layout = 'cabinet';
            $query->andWhere(['user_id' => $user_id]);
        }
        return $this->render('/invoice/index', [
            'dataProvider' => new ActiveDataProvider([
                'query' => $query,
                'sort' => [
                    'defaultOrder' => ['id' => SORT_DESC]
                ]
            ])
        ]);
    }

    public function actionView($id) {
        return $this->render('view', [
            'model' => $this->findModel($id)
        ]);
    }

    public function actionPayment() {
        return $this->edit(new Invoice([
            'withdraw' => false,
            'user_id' => Yii::$app->user->id
        ]));
    }

    public function actionWithdraw() {
        return $this->edit(new Invoice([
            'withdraw' => true,
            'user_id' => Yii::$app->user->id
        ]));
    }

    public function actionChoose($id) {
        return $this->edit($this->findModel($id));
    }

    public function actionUpdate($id) {
        return $this->edit($this->findModel($id));
    }

    public function actionSuccess($id) {
        $transaction = Yii::$app->db->beginTransaction();
        $model = $this->findModel($id);
//        $this->logPayment($model);
        if ('success' == $model->status) {
            $transaction->rollBack();
            Yii::$app->session->addFlash('error', Yii::t('app', 'Payment has already done previously'));
            return $this->redirect(['view', 'id' => $model->id]);
        }
        elseif ($model->withdraw && $model->user->account < $model->amount) {
            Yii::$app->session->addFlash('error', Yii::t('app', 'Insufficient funds'));
        }
        else {
            try {
                if ($this->success($model)) {
                    $model->status = 'success';
                    if ($model->save()) {
                        $amount = (float) $model->amount;
                        if ($model->withdraw) {
                            $amount = -$amount;
                        }
                        $model->user->account += $amount;
                        if ($model->user->save(true, ['account'])) {
                            $transaction->commit();
                            return $this->redirect(['/invoice/view', 'id' => $model->id]);
                        }
                    }
                    else {
                        $model->dumpErrors();
                    }
                }
            } catch (\Exception $ex) {
                Yii::$app->session->addFlash('error', $ex->getMessage());
            }
        }

        $transaction->rollBack();
        return $this->render('@app/views/invoice/info');
    }

    /**
     * @param Invoice $model
     * @return bool
     */
    protected function success(Invoice $model) {
        throw new NotSupportedException();
    }

    protected function edit(Invoice $model) {
        if (isset($_GET['user_id'])) {
            Yii::$app->layout = 'cabinet';
        }
        if ('invoice' != $this->id) {
            $model->type = $this->id;
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->withdraw && $model->user->account < $model->amount) {
                $model->addError('amount', Yii::t('app', 'Insufficient funds'));
            }
            else {
                if ($model->save()) {
//                    $params = $model->withdraw ? ['view'] : [$model->type . '/' . $model->type . '/pay'];
//                    $params['id'] = $model->id;
//                    return $this->redirect($params);
                    return $this->render('freekassa2',[
                        'order_amount'=>$model->amount,
                        'user'=>$model->user_id,
                        'order_id'=>$model->id
                    ]);
                }
                else {
                    $model->dumpErrors();
                }
            }
        }
        return $this->render('/invoice/edit', [
            'model' => $model
        ]);
    }

    protected function log(Invoice $invoice, $extra = null) {
        $invoice_data = [];
        foreach($invoice->attributes as $key => $value) {
            if (null !== $value) {
                $invoice_data[$key] = $value;
            }
        }
        $data = [
            'invoice' => $invoice_data
        ];
        if (!empty($_GET)) {
            $data['get'] = &$_GET;
        }
        if (!empty($_POST)) {
            $data['post'] = &$_POST;
        }
        if ($extra) {
            $data['extra'] = $extra;
        }
        $date = date("y-m-d_H-i-s");
        $action = Yii::$app->controller->action->id;
        file_put_contents(Yii::getAlias("@app/log/$invoice->type/$action/$date.$invoice->user_id.json"),
            json_encode($data, JSON_PRETTY_PRINT));
    }

    protected function findModel($id) {
        /** @var Invoice $model */
        $model = Invoice::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException();
        }
        return $model;
    }
    public function actionFreekassa($notify = null){
        if($notify == '1'){
            return $this->render('info',['success'=>true]);
        }
        if($notify == '0'){
            return $this->render('info',['success'=>false]);
        }
        if($_GET['SIGN'] == md5($_REQUEST['MERCHANT_ID'].':'.$_REQUEST['AMOUNT'].':a5o03sor:'.$_REQUEST['MERCHANT_ORDER_ID'])){
            $invoice = $this->findModel($_GET['MERCHANT_ORDER_ID']);
            $invoice->status = 'success';
            $invoice->batch = $_GET['intid'];
            $invoice->wallet = $_GET['CUR_ID'];
            $invoice->save();
            Yii::$app->db->createCommand('UPDATE "user" SET account = account + '.$_GET['AMOUNT'].' WHERE id='."'".$_GET['us_login']."'")
                ->execute();
            return $this->redirect(['view','id'=>$invoice->id]);
        }else{
            return $this->redirect(['freekassa','notify'=>0]);
        }
    }
}

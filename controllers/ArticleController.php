<?php

namespace app\controllers;

use app\helpers\ArrayHelper;
use app\models\Article;
use app\models\ArticleTree;
use app\models\ArticleView;
use app\models\Hit;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\imagine\Image;
use yii\web\Response;
use yii\web\UploadedFile;


class ArticleController extends TextController
{
    protected function findModel() {
        $params = $this->primaryKey();
        if (isset($params['id'])) {
            $model = Article::findOne($params);
        }
        else {
            $model = new Article([
                'lang' => Yii::$app->language,
                'id' => rand(1, 10000000)
            ]);
        }
        return $model;
    }

    public function actionIndex() {
        $query = ArticleTree::find();
        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider([
                'query' => $query->andWhere('count <= 0')
                    ->andFilterWhere(ArrayHelper::filter(['root_id', 'parent_id'])),
                'pagination' => [
                    'pageSize' => 16,
                ],
            ]),
            'categories' => static::categories(),
            'layout' => isset($_GET['layout']) ? $_GET['layout'] : 'list'
        ]);
    }

    protected static function categories() {
        $query = ArticleTree::find();
        $categories = null;
        if (!empty($_GET)) {
            if (isset($_GET['root_id'])) {
                $categories = ArticleView::find()
                    ->andWhere(['parent_id' => $_GET['root_id']])
                    ->andWhere('count > 0')
                    ->orderBy(['id' => SORT_ASC])
                    ->all();
                if(!$categories) {
                    $query1 = new Query();
                    $query1->select('id')->from('article')->where(['parent_id' => $_GET['root_id']]);
                    $ctg = $query1->one();
                    if ($ctg) {
                        $query1 = new Query();
                        $query1->select('parent_id')->from('article')->where(['id' => $_GET['root_id']]);
                        $ctg = $query1->one();
                        $categories = ArticleView::find()
                            ->andWhere(['parent_id' => $ctg['parent_id']])
                            ->andWhere('count > 0')
                            ->orderBy(['id' => SORT_ASC])
                            ->all();
                    }
                }
            }
            $query->andFilterWhere(ArrayHelper::filter(['root_id', 'parent_id', 'lang', 'number']));
        }
        return $categories;
    }

    protected function find() {
        return Article::find();
    }

    protected static function save($model)
    {
        if ($model->save()) {
            $upload = UploadedFile::getInstance($model, 'image');
            if ($upload) {
                /** @var \Imagine\Image\ImagineInterface $image */
                $image = Image::getImagine()->open($upload->tempName);
                /** @var \Imagine\Image\BoxInterface $size */
                $size = $image->getSize();
                if ($size->getWidth() > 1280 || $size->getHeight() > 920) {
                    $size = $size->getWidth() > $size->getHeight()
                        ? $size->widen(1280)
                        : $size->heighten(920);
                    $image->resize($size);
                }
                $filename = $model->safeId() . '.jpg';
                $image->save(Yii::getAlias('@app/web/data/article/image/' . $filename));
                $thumbBox = $size->getWidth() > $size->getHeight()
                    ? $size->widen(192)
                    : $size->heighten(128);
                $image->resize($thumbBox);
                $image->save(Yii::getAlias('@app/web/data/article/thumb/' . $filename));
            }
            return true;
        }
        return false;
    }

    public function actionSearch($title) {
        unset($_GET['title']);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $query = Article::find()
            ->andFilterWhere($_GET)
            ->andWhere('title like :title', [':title' => "%$title%"])
            ->limit(10)
            ->select('id, title');
        $result = [];
        foreach($query->each() as $article) {
            $result[$article->id] = $article->title;
        }
        return $result;
    }

    public function actionTop() {
        return $this->render('top', [
            'lists' => [
                Yii::t('app', 'Novelty') => Article::find()
                    ->select('id, title, parent_id')
                    ->where('price is not null')->orderBy(['parent_id' => SORT_ASC])->all(),
                Yii::t('app', 'Hits for sales') => Article::find()
                    ->where('price is not null')->orderBy(['id' => SORT_DESC])->limit(5)->all(),
            ],
            'categories' => static::categories(),
        ]);
    }
    public function actionGallery(){
        $model = Article::find()->select('title,content')->where(['parent_id'=>'gallery'])->orderBy(['title' => SORT_ASC])->all();
        return $this->render('gallery',[
           'model'=>$model
        ]);
    }
}

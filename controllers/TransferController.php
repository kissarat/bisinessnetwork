<?php

namespace app\controllers;


use app\helpers\ArrayHelper;
use app\models\Transfer;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

class TransferController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create'],
                'rules' => [
                    [
                        'actions' => ['index', 'create'],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ]
            ]
        ];
    }

    public function actionIndex()
    {
        if (!Yii::$app->user->can('manage')) {
            if (!ArrayHelper::equalsAny(['user_id', 'object_id'], Yii::$app->user->id)) {
                throw new ForbiddenHttpException(Yii::t('app', 'You can see only your own transfers'));
            }
            Yii::$app->layout = 'cabinet';
        }
        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider([
                'query' => Transfer::find()->filterWhere(ArrayHelper::filter([
                    'user_id', 'type', 'event', 'object_id', 'ip'])),
                'sort' => [
                    'defaultOrder' => ['time' => SORT_DESC]
                ]
            ])
        ]);
    }

    public function actionCreate()
    {
        $model = new Transfer();

        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->user->identity->account < $model->amount) {
                $model->addError('amount', Yii::t('app', 'Insufficient funds'));
            } else {
                $model->type = 'user';
                $model->event = 'transfer';
                $model->user_id = Yii::$app->user->id;
                $model->ip = Yii::$app->request->getUserIP();
                $transaction = Yii::$app->db->beginTransaction();
                $model->receiver->account += $model->amount;
                $model->user->account -= $model->amount;
                if ($model->receiver->save(true, ['account'])
                    && $model->user->save(true, ['account'])
                    && $model->save()
                ) {
                    $transaction->commit();
                    return $this->redirect(['index',
                        'type' => 'user',
                        'event' => 'transfer',
                        'user_id' => $model->user_id]);
                }
            }
        }

        Yii::$app->layout = 'cabinet';
        return $this->render('create', ['model' => $model]);
    }
}

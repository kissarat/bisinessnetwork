<?php

namespace app\controllers;


use app\helpers\ArrayHelper;
use app\models\Invoice;
use app\models\Order;
use app\models\OrderView;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class OrderController extends Controller
{
    public function actionIndex($user_id = null) {
        if (!Yii::$app->user->can('manage')) {
            Yii::$app->layout = 'cabinet';
        }
        $query = OrderView::find();
        if (!empty($_GET)) {
            $query->andFilterWhere(ArrayHelper::filter(['user_id', 'status', 'product_id']));
        }
        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider([
                'query' => $query,
                'sort' => [
                    'defaultOrder' => ['id' => SORT_DESC]
                ]
            ]),
            'user_id' => $user_id
        ]);
    }

    public function actionEdit($id = null) {
        if(Yii::$app->user->isGuest){
           return $this->redirect('/user/login');
        }
        $model = $id
            ? $this->findModel($id)
            : new Order(ArrayHelper::filter(['product_id', 'invoice_id', 'user_id']));
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $query = new Query();
//            $query->select('id')->from('invoice')->where(['user_id'=>$model->user_id]);
//            $invoice = $query->one();
            $invoice = new Invoice([
                'user_id' => $model->user_id,
                'amount' => $model->product->price,
                'withdraw' => false,
                'status'=>'buy'
            ]);
           if ($invoice->save()) {
                $model->invoice_id = $invoice->id;
           // $query = new Query();
            $query->select('account')->from('user')->where(['id'=>$model->user_id]);
            $account = $query->one();
            $query->select('price')->from('article')->where(['id'=>$model->product_id]);
            $price = $query->one();
            if($account['account']<$price['price']){
                return $this->redirect(['/invoice/payment','user_id'=>$model->user_id]);
            } else{
                Yii::$app->db->createCommand('UPDATE "user" SET account = account - '.$price['price'].' WHERE id='."'".$model->user_id."'")
                    ->execute();
            }
                if ($model->save()) {
                    return $this->redirect(['/order/index',
                        'user_id'=>$model->user_id
                    ]);
                }
            }
            else {
                $invoice->dumpErrors();
            }
        }
        return $this->render('edit', [
            'model' => $model
        ]);
    }

    public function actionComplete($id) {
        $model = $this->findModel($id);
        $transaction = Yii::$app->db->beginTransaction();
        if ($model->product->price >= $model->user->account) {
            $model->user->account -= $model->product->price;
            if ($model->user->save(true, ['account'])) {
                $transaction->commit();
                Yii::$app->session->addFlash('success', Yii::t('app', 'Order {order} #{id} completed', [
                    'order' => Html::a($model->product->title, ['view', 'id' => $model->id]),
                    'id' => $model->id
                ]));
            }
        }
        else {
            Yii::$app->session->addFlash('error', Yii::t('app', 'Insufficient funds'));
        }
        if ($transaction->getIsActive()) {
            $transaction->rollBack();
            Yii::$app->session->addFlash('error',
                Yii::t('app', 'Order {order} #{id} failed', [
                'order' => Html::a($model->product->title, ['view', 'id' => $model->id]),
                'id' => $model->id
            ]));
        }
        return $this->redirect(['index', 'user_id' => $model->id]);
    }

    /**
     * @param $id
     * @return Order
     * @throws NotFoundHttpException
     */
    protected function findModel($id) {
        /** @var Order $model */
        $model = Order::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException();
        }
        return $model;
    }
}

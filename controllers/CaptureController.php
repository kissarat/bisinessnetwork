<?php
/**
 * Created by PhpStorm.
 * User: yura
 * Date: 17.02.16
 * Time: 11:21
 */
namespace app\controllers;

use Yii;
use app\models\Capture;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\web\ForbiddenHttpException;

/**
 * PostsController implements the CRUD actions for Posts model.
 */
class CaptureController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
//                'actions' => [
//                    'delete' => ['post'],
//                ],
            ],
        ];
    }



    /**
     * Displays a single Posts model.
     * @param string $user_id
     * @return mixed
     */
    public function actionCapturepage($user_id)
    {
        Yii::$app->layout = 'capture';
        $query = new Query();
        $query->select('content,user_id')->from('capture')->where(['user_id'=>$user_id]);
        $capture = $query->one();
        return $this->render('capturepage',[
            'capture'=>$capture
        ]);
    }

    /**
     * Updates an existing Posts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $user_id
     * @return mixed
     */
    public function actionCaptureedit($user_id)
    {
        Yii::$app->layout = 'cabinet';

        $model = $this->findModel($user_id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->render('captureedit',[
                'model'=> $model,
                'succ' =>true
            ]);

        }
//        $query = new Query();
//        $query->select('capture')->from('user')->where(['id'=>$user_id]);
//        $capture = $query->one();
        return $this->render('captureedit',[
            'model'=> $model
        ]);
    }

    /**
     * Finds the Posts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $user_id
     * @return Capture the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($user_id)
    {
        if (($model = Capture::findOne($user_id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
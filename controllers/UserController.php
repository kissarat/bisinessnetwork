<?php

namespace app\controllers;


use app\models\Capture;
use app\models\LoginForm;
use app\models\PasswordForm;
use app\models\RequestForm;
use app\models\User;
use app\models\WalletForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class UserController extends Controller {
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'login', 'logout', 'request', 'reset', 'password', 'reset-auth'],
                'rules' => [
                    [
                        'actions' => ['create', 'login', 'request', 'reset'],
                        'allow' => true,
                        'roles' => ['?']
                    ],
                    [
                        'actions' => ['logout', 'update', 'password', 'reset-auth'],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ]
            ]
        ];
    }

    public function actionIndex($referral_id = null) {
        if ($referral_id) {
            Yii::$app->layout = 'cabinet';
        }
        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider([
                'query' => User::find()->andFilterWhere(['referral_id' => $referral_id]),
                'sort' => [
                    'defaultOrder' => ['id' => SORT_DESC]
                ]
            ])
        ]);
    }

    public function actionLogin() {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = $model->getUser();
            if ($user && $user->validatePassword($model->password)) {
                if (empty($user->auth)) {
                    $user->generateAuthKey();
                    $user->save(true, ['auth']);
                }

                if (Yii::$app->user->login($user)) {
                    $isLocal = Yii::$app->request->getServerName() == parse_url(Yii::$app->request->referrer, PHP_URL_HOST);
                    if (isset($_POST['back']) && $isLocal) {
                        return $this->redirect(Yii::$app->request->referrer);
                    }
                    return Yii::$app->user->can('manage')
                        ? $this->redirect(['/user/index'])
                        : $this->redirect(['view', 'id' => $user->id]);
                }
            }
            else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Invalid username or password'));
            }
        }

        return $this->render('login', [
            'model' => $model
        ]);
    }

    public function actionLogout($destroy = false) {
        Yii::$app->user->logout($destroy);
        return $this->goHome();
    }

    public function actionResetAuth($id) {
        if (!Yii::$app->user->can('manage') && $id != Yii::$app->user->id) {
            throw new ForbiddenHttpException();
        }
        $model = $this->findModel($id);
        $model->generateAuthKey();
        if ($model->save(false)) {
            Yii::$app->session->addFlash('success', Yii::t('app', 'Logout done'));
        }
        return $this->goHome();
    }

    public function actionCreate($referral_id = null) {
        $model = new User(['referral_id' => $referral_id]);
        Yii::$app->view->title = Yii::t('app', 'Signup');
        return $this->edit($model);
    }

    public function actionUpdate($id = null) {
        $model = $this->findModel($id);
        Yii::$app->view->title = Yii::t('app', 'Update');
        Yii::$app->layout = 'cabinet';
        return $this->edit($model);
    }

    public function actionView($id = null) {
        if (empty($id)) {
            return $this->redirect(['user/view', 'id' => Yii::$app->user->id]);
        }
        $model = $this->findModel($id);
        Yii::$app->layout = 'cabinet';
        return $this->render('view', [
            'model' => $model
        ]);
    }

    public function findModel($id = null) {
        /** @var User $model */
        if ($id) {
            $model = User::findOne($id);
            if (!$model) {
                throw new NotFoundHttpException();
            }
        }
        else {
            $model = Yii::$app->user->identity;
        }
        return $model;
    }

    public function actionPassword($id) {
        if (!Yii::$app->user->can('manage') && $id != Yii::$app->user->id) {
            throw new ForbiddenHttpException();
        }
        $user = $this->findModel($id);
        $model = new PasswordForm();
        if ($this->password($user, $model)) {
            return $this->redirect(['view', 'id' => $id]);
        }
        Yii::$app->layout = 'cabinet';
        return $this->render('password', [
            'model' => $model
        ]);
    }

    public function actionReset($code) {
        /** @var User $user */
        $user = User::findOne(['code' => $code]);
        if (!$user) {
            Yii::$app->session->addFlash('error', Yii::t('app', 'Invalid code'));
            return $this->goHome();
        }
        $model = new PasswordForm();
        $user->code = null;
        if ($this->password($user, $model)) {
            return $this->redirect(['login']);
        }
        return $this->render('password', [
            'model' => $model
        ]);
    }

    public function actionRequest() {
        $model = new RequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = $model->getUser();
            if ($user) {
                $user->generateCode();
                if ($user->save(false)) {
                    $url = Url::to(['reset', 'code' => $user->code], true);
                    $content = Yii::t('app', 'To recover your password, open <a href="{url}">this link</a>', [
                        'url' => $url,
                    ]);
                    $user->sendEmail([
                        'subject' => Yii::$app->name . ': ' . Yii::t('app', 'Password reset'),
                        'content' => $content
                    ]);
                    Yii::$app->session->addFlash('info', Yii::t('app', 'Check your mail'));
                    return $this->goHome();
                }
            }

            if (empty($model->id) && empty($model->email)) {
                Yii::$app->session->addFlash('error', Yii::t('app', 'Enter login or email'));
            }
        }

        return $this->render('request', [
            'model' => $model
        ]);
    }

    public function actionWallet($id) {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save(true, $model->walletAttributes())) {
            return $this->redirect(['view', 'id' => $id]);
        }
        Yii::$app->layout = 'cabinet';
        return $this->render('wallet', [
            'model' => $model
        ]);
    }

    public function actionComplete($search) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return User::find()
            ->select('id')
            ->where('id like :id', [
                ':id' => "$search%"
            ])
            ->limit(10)
            ->column();
    }
    protected function edit(User $model) {
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->save(false) && Yii::$app->user->login($model)) {
                $query = new Query();
                $query->select('user_id')->from('capture')->where(['user_id'=>$model->id]);
                $capture = $query->one();
                if(!$capture) {
                    $capture = new Capture();
                    $capture->user_id = $model->id;
                    $capture->content = $model->id." referral page";
                    $capture->save();
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('edit', [
            'model' => $model
        ]);
    }

    protected function password(User $user, PasswordForm $model) {
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user->setPassword($model->password);
            return $user->save(true, ['hash', 'auth', 'code']);
        }
        return false;
    }
}

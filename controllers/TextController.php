<?php

namespace app\controllers;


use app\helpers\ArrayHelper;
use app\helpers\SQL;
use app\models\Article;
use app\models\Text;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

class TextController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['edit'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin']
                    ]
                ]
            ]
        ];
    }

    public function actionEdit()
    {
        $model = $this->findModel();
        if ($model->load(Yii::$app->request->post()) && static::save($model)) {
            if (false === strpos($model->id, '/')) {
                $this->redirect(['view', 'id' => $model->id, 'lang' => $model->lang]);
            }
            else {
                $this->redirect('/' . $model->id);
            }
        }
        return $this->render('edit', [
            'model' => $model
        ]);
    }

    public function actionView()
    {
        $model = $this->findModel();
        return $this->render('view', [
            'model' => $model
        ]);
    }

    public function actionIndex() {
        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider([
                'query' => static::find(),
            ])
        ]);
    }

    protected function findModel() {
        $params = $this->primaryKey();
        $model = Text::findOne($params);
        if (!$model) {
            $model = new Text($params);
        }
        return $model;
    }

    protected function find() {
        return Text::find();
    }

    protected static function save($model) {
        return $model->save();
    }

    protected function primaryKey($array = []) {
        if (isset($_GET['id'])) {
            $array['id'] = $_GET['id'];
        }
        $array['lang'] = empty($_GET['lang']) ? Yii::$app->language : $_GET['lang'];
        return $array;
    }
}

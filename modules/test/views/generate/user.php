<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var \app\modules\test\models\UserGeneratorForm $gen */
/** @var \app\models\User[] $models */
?>
<div class="generate-user">
    <?php $form = ActiveForm::begin() ?>

    <?= $form->field($gen, 'referral_id') ?>
    <?= $form->field($gen, 'prefix') ?>
    <?= $form->field($gen, 'count') ?>
    <?= Html::submitButton(Yii::t('app', 'Generate')) ?>

    <?php ActiveForm::end() ?>

    <?php
    $items = [];
    foreach($models as $model) {
        $items[] = implode(' ', [
            Html::a($model->id, ['/user/view', 'id' => $model->id]),
            Html::a($model->referral_id, ['/user/view', 'id' => $model->referral_id]),
        ]);
    }
    echo Html::ul($items, ['encode' => false]);
    ?>
</div>

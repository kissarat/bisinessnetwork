<?php

namespace app\modules\test\models;


use app\models\Order;
use Faker\Generator;

class OrderFake extends Order implements FakeInterface
{
    use FakeTrait;

    public static function generateData(Generator $factory) {
        return [
            'email' => UserFake::randomId() . '@yopmail.com',
            'surname' => $factory->firstName,
            'forename' => $factory->lastName,
            'patronymic' => $factory->firstNameMale,
            'skype' => $factory->userName,
            'phone' => $factory->phoneNumber,
            'postal' => $factory->postcode,
            'city' => $factory->city,
            'address' => $factory->address,
            'country' => strtoupper($factory->countryCode),
        ];
    }
}

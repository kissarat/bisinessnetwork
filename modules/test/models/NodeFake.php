<?php

namespace app\modules\test\models;


use app\helpers\SQL;
use app\modules\matrix\models\Node;
use Faker\Generator;

class NodeFake extends Node implements FakeInterface
{
    use FakeTrait;

    public static function generateData(Generator $factory) {
        $type_id = rand(1, 3);
        $user_id = SQL::queryCell('SELECT id FROM "user" EXCEPT
          SELECT user_id FROM matrix.node WHERE type_id = :type_id', [
            ':type_id' => $type_id
        ]);
//        $node = Node::findOne($type_id);
        return [
            'user_id' => $user_id,
            'type_id' => $type_id,
//            'parent_id' => $node->findFree(),
        ];
    }
}

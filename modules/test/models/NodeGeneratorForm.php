<?php

namespace app\modules\test\models;


use app\models\User;
use Faker\Factory;
use yii\base\Model;

class NodeGeneratorForm extends Model
{
    public $count;

    public function rules() {
        return [
            ['count', 'integer']
        ];
    }

    public function generate() {
        $factory = Factory::create();
        $models = [];
        if (empty($this->count)) {
            $this->count = User::find()->count() - 1;
        }
        for($i = 0; $i < $this->count; $i++) {
            try {
                $model = new NodeFake();
                $model->fake($factory);
                $model->open(false);
                $models[] = $model;
            }
            catch(\Exception $ex) {}
        }
        return $models;
    }
}

<?php

namespace app\modules\test\models;


use app\helpers\SQL;
use app\models\User;
use Faker\Generator;

class UserFake extends User implements FakeInterface
{
    use FakeTrait;

    public static function generateData(Generator $factory) {
        $login = $factory->userName;
        return [
            'referral_id' => static::randomId(),
            'id' => $factory->userName,
            'surname' => $factory->firstName,
            'forename' => $factory->lastName,
            'patronymic' => $factory->firstNameMale,
            'skype' => $factory->userName,
            'phone' => $factory->phoneNumber,
            'postal' => $factory->postcode,
            'city' => $factory->city,
            'address' => $factory->address,
            'country' => strtoupper($factory->countryCode),
            'pin' => rand(1000,9999),
            'email' => $login . '@yopmail.com',
            'password' => 1,
            'repeat' => 1,
            'confidant_type' => array_rand(User::getConfidantTypes()),
            'confidant_surname' => $factory->firstName,
            'confidant_forename' => $factory->lastName,
            'confidant_patronymic' => $factory->firstNameMale,
            'account' => 300
        ];
    }

    public static function randomId() {
        return SQL::queryCell('SELECT id FROM "user" ORDER BY RANDOM() LIMIT 1');
    }
}

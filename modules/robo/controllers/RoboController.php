<?php

namespace app\modules\robo\controllers;


use app\controllers\InvoiceController;
use app\models\Invoice;
use app\models\Config;
use Yii;
use yii\web\Response;

class RoboController extends InvoiceController
{
    public function actionIndex($user_id = null)
    {
        return 'Page in development. Waiting for shop approvement';
    }

    public function actionSuccess($id = null)
    {
        return 'Page in development. Waiting for shop approvement';
    }

    public function actionFail($id = null)
    {
        return 'Page in development. Waiting for shop approvement';
    }

    public function actionResult($id = null)
    {
        return 'Page in development. Waiting for shop approvement';
    }
}

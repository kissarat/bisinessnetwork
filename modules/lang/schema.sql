CREATE VIEW "translation" AS
  SELECT s.id, message, translation
  FROM source_message s JOIN message t ON s.id = t.id
  WHERE "language" = 'ru';

<?php

namespace app\modules\perfect\controllers;


use app\controllers\InvoiceController;
use app\helpers\PerfectHelper;
use app\models\Invoice;
use app\models\Config;
use Yii;

class NixController extends InvoiceController
{
    public function actionPay($id) {
        return $this->render('pay', [
            'model' => $this->findModel($id)
        ]);
    }

    protected function success(Invoice $model) {
        if (!$model->withdraw) {
                return true;
        }
        return false;
    }

    public function actionStatus($id) {
        $model = $this->findModel($id);
        if ($this->validate()) {
            $model->wallet = $_POST['PAYER_ACCOUNT'];
            $model->batch = $_POST['PAYMENT_BATCH_NUM'];
            $model->amount = (float) $_POST['PAYMENT_AMOUNT'];
        }
    }

    protected function validate() {
        return PerfectHelper::validate(Config::get('perfect', 'wallet'), Config::get('perfect', 'password'));
    }
}

<?php
/** @var \app\models\Invoice $model */

use app\models\Config;
use yii\helpers\Url;

?>
<form id="nix" action="https://nixmoney.com/merchant.jsp" method="post">
    <input name="PAYMENT_ID" type="hidden" value="<?= $model->id ?>"/>
    <input name="PAYEE_ACCOUNT" value="<?= Config::get('nix', 'wallet') ?>" type="hidden"/>
    <input name="PAYMENT_AMOUNT" type="hidden" value="<?= $model->amount ?>"/>
    <input name="PAYEE_NAME" value="<?= Yii::$app->name ?>" type="hidden"/>
    <input name="PAYMENT_UNITS" value="EUR" type="hidden"/>
    <input name="STATUS_URL" value="<?= Url::to(['/nix/nix/status', 'id' => $model->id])?>" type="hidden"/>
    <input name="PAYMENT_URL" value="<?= Url::to(['/nix/nix/success', 'id' => $model->id])?>" type="hidden"/>
    <input name="NOPAYMENT_URL" value="<?= Url::to(['/nix/nix/fail', 'id' => $model->id])?>" type="hidden"/>
    <input name="BAGGAGE_FIELDS" value="USER_NAME" type="hidden"/>
    <input name="USER_NAME" value="<?= $model->user_id ?>" type="hidden"/>
    <input name="PAYMENT_METHOD" type="submit" value="NixMoney"/>
</form>

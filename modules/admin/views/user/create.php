<?php
/* @var $this yii\web\View */
/* @var $model \app\models\User */

use app\widgets\Fake;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="admin-user-create">
    <?php
    $form = ActiveForm::begin([
        'options' => ['name' => 'User']
    ]);
    ?>

    <p>
        <?= Fake::widget() ?>
    </p>

    <div>
        <?= $form->field($model, 'referral_id') ?>
        <?= $form->field($model, 'id') ?>
        <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'account') ?>
    </div>

    <p>
        <?= Html::submitButton(Yii::t('app', 'Create'), ['class' => 'button']) ?>
    </p>

    <?php ActiveForm::end(); ?>
</div>

<?php

namespace app\modules\perfect\controllers;


use app\controllers\InvoiceController;
use app\helpers\PerfectHelper;
use app\models\Invoice;
use app\models\Config;
use Yii;
use yii\web\Response;

class PerfectController extends InvoiceController
{
    public function actionPay($id) {
        return $this->render('pay', [
            'model' => $this->findModel($id)
        ]);
    }

    public function actionHash() {
        $data = file_get_contents('php://input');
        $data = str_replace("\n", ":", $data);
        Yii::$app->response->format = Response::FORMAT_RAW;
        return strtoupper(md5($data));
    }

    protected function success(Invoice $model) {
        if (!$model->withdraw) {
            if ($this->validate()) {
                $model->wallet = $_POST['PAYER_ACCOUNT'];
                $model->batch = $_POST['PAYMENT_BATCH_NUM'];
                $model->amount = (float) $_POST['PAYMENT_AMOUNT'];
                return true;
            }
        }
        return false;
    }

    protected function validate() {
        return PerfectHelper::validate(Config::get('perfect', 'wallet'), Config::get('perfect', 'secret'));
    }
}

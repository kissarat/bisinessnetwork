<?php

namespace app\modules\matrix\models;


use app\helpers\SQL;
use app\models\Transfer;
use app\models\User;
use app\models\UserRecord;
use League\Flysystem\Exception;
use Yii;
use yii\db\ActiveQuery;
use yii\db\Query;

/**
 * @property integer id
 * @property integer type_id
 * @property integer parent_id
 * @property string user_id
 *
 * @property Type type
 * @property User user
 * @property Node parent
 * @property Node[] children
 */
class Node extends UserRecord {

    public function rules() {
        return [
            [['type_id', 'user_id'], 'required'],
            [['parent_id', 'type_id'], 'integer'],
        ];
    }

    public static function tableName() {
        return 'matrix.node';
    }

    public function getType() {
        return $this->hasOne(Type::className(), ['id' => 'type_id']);
    }

    public function getParent() {
        return $this->hasOne(Node::className(), ['id' => 'parent_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getChildren() {

        return $this->hasMany(Node::className(), [
            'parent_id' => 'id'
        ]);
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'parent_id' => Yii::t('app', 'Parent'),
            'type_id' => Yii::t('app', 'Program'),
            'user_id' =>  Yii::t('app', 'User'),
        ];
    }

    public function __toString() {
        return $this->user_id;
    }

    public function getName() {
        return $this->__toString() . ' (' . $this->id . ')';
    }
    /**
     * @param integer $depth
     * @return array
     */
    private function treeChild(){
        $query = new Query;
        $query->select('id')->from('user')->where(['referral_id'=>$this->user_id]);
        $children = $query->all();
        $models = [];
        foreach($children as $child){
            $model = Node::find()->where(['user_id'=>$child['id']])->orderBy('type_id DESC')->one();
            if($model) {
                $models[] = $model;
            }
        }
       return $models;
    }
    public function tree($depth) {
        $nodes = [];
        if ($depth > 0) {
            /** @var Node[] $children */
            $children = $this->treeChild();
            //$children = $this->getChildren()->orderBy(['id' => SORT_ASC])->all();
            foreach ($children as $child) {
                $nodes[] = $child->tree($depth - 1);
            }
        }

        $info = ['name' => $this->user_id,'type_id'=>$this->type_id];
        if (count($nodes) > 0) {
            $info['children'] = $nodes;
        }
        return $info;
    }

    /**
     * @return integer
     */
    public function findFree() {
        return SQL::queryCell('SELECT matrix.descend(:root_id)', [
            ':root_id' => $this->id
        ]);
    }

    public function findParent() {
        $referral = $this->user;
        while ($referral->referral_id) {
            $referral = $referral->referral;
            /** @var static $candidate */
            $candidate = $referral->getNodes()
                ->where(['type_id' => $this->type_id])
                ->orderBy(['id' => SORT_ASC])
                ->one();
            if ($candidate) {
                $this->parent_id = $candidate->findFree();
                break;
            }
        }
    }

    public function open($debit = true) {
        $this->findParent();
        if (!Node::find()->where(['user_id' => User::findOne($this->user_id)->referral_id, 'id' => $this->parent_id])->exists()) {
            $this->parent_id = null;
        }
        return $this->enter($debit);
    }

    public function accrue(Node $sponsor_node = null) {
        $amount = $this->type->price * 0.1;
        $user_id = $sponsor_node ? $sponsor_node->user_id : null;
        if (!Transfer::find()->where(['type' => 'node', 'event' => 'accrue', 'object_id' => $this->id,
            'user_id' => $user_id])->exists()) {
            $this->user->account += $amount;
            if ($this->user->save(false)) {
                return $this->log('accrue', $amount, $user_id);
            }
            return false;
        }
        return true;
    }

    public function enter($debit = true) {
        if ($debit) {
            $this->user->account -= $this->type->price;
            if (!$this->user->save(false)) {
                return false;
            }
        }
        if ($this->save()) {
            /** @var static $node */
            foreach($this->getTree()->each() as $node) {
                if (!$node->loopAccrue()) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public function loopAccrue() {
        /** @var Referral $node */
        foreach($this->getReferral()->each() as $node) {
            if ($node->referral_level > 1) {
                if (!$node->accrue($this)) {
                    return false;
                }
            }
            elseif (1 == $node->referral_level) {
                if ($node->getChildren()->count() == 2) {
                    foreach($node->children as $child) {
                        $node->accrue($child);
                    }
                    $node->accrue();
                }
            }
        }
        return true;
    }

    public function log($event, $amount, $user_id = null) {
        $transfer = new Transfer([
            'type' => 'node',
            'event' => $event,
            'user_id' => $user_id,
            'object_id' => $this->id,
            'amount' => $amount,
            'ip' => Yii::$app->request->getUserIP(),
        ]);
        if ($transfer->save()) {
            return $transfer;
        }
        return false;
    }

    public function getIncome() {
        return $this->hasOne(Income::className(), ['id' => 'id']);
    }

    public function getTree() {
        return $this->hasMany(Tree::className(), ['root_id' => 'id']);
    }

    public function getReferral() {
        return $this->hasMany(Referral::className(), ['sponsor_id' => 'id']);
    }
}

<?php

namespace app\modules\matrix\models;

/**
 * @property integer type_id
 * @property string user_id
 * @property string referral_id
 * @property integer level
 * @property integer referral_level
 * @property number interest
 * @property string root
 * @property string time
 */
class Referral extends Node
{
    public static function tableName()
    {
        return 'matrix.referral';
    }
}

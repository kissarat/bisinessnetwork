<?php

namespace app\modules\matrix\models;


trait UserNodeTrait
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNodes() {
        return $this->hasMany(Node::class, ['user_id' => 'id']);
    }

    public function getAvailableTypes() {
        return Type::findBySql('SELECT * FROM matrix.type WHERE id NOT IN
          (SELECT type_id FROM matrix.node WHERE user_id = :user_id) AND price <= :amount', [
            ':user_id' => $this->id,
            ':amount' => (int) $this->account,
        ]);
    }
}

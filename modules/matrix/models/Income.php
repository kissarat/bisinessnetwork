<?php

namespace app\modules\matrix\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer $amount
 */
class Income extends ActiveRecord
{
    public static function tableName()
    {
        return 'matrix.income';
    }

    public function attributeLabels()
    {
        return [
            'amount' => Yii::t('app', 'Income')
        ];
    }
}

<?php


namespace app\modules\matrix\models;


use app\models\Transfer;

/**
 * @property integer $level
 */
class Accrue extends Transfer
{
    public static function tableName()
    {
        return 'matrix.accrue';
    }
}

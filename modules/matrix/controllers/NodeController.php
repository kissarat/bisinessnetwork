<?php

namespace app\modules\matrix\controllers;

use app\helpers\SQL;
use app\models\User;
use app\modules\matrix\models\Accrue;
use app\modules\matrix\models\Node;
use app\modules\matrix\models\Type;
use PDO;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\web\Controller;
use Yii;
use yii\web\NotFoundHttpException;

class NodeController extends Controller
{
    public function actionIndex($user_id = null) {
        if ($user_id) {
            Yii::$app->layout = 'cabinet';
        }
        $query = Node::find()->joinWith('income')->filterWhere(['user_id' => $user_id]);
        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider([
                'query' => $query,
                'sort' => [
                    'defaultOrder' => ['id' => $user_id ? SORT_ASC : SORT_DESC]
                ]
            ]),
            'sum' => $query->sum('income.amount'),
            'canOpen' => $user_id ? User::findOne($user_id)->getAvailableTypes()->exists() : false
        ]);
    }

    public function actionCreate($type_id = null) {
        if (isset($_GET['user_id'])) {
            Yii::$app->layout = 'cabinet';
        }

        $model = new Node([
            'type_id' => (int)$type_id,
            'user_id' => Yii::$app->user->id
        ]);
        return $this->edit($model);
    }

    public function edit(Node $model) {
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $transaction = Yii::$app->db->beginTransaction();
            $type = Type::findOne($model->type_id);
            if ($model->user->account >= $type->price
                && $model->log('buy', $type->price)
                && $model->open(true)) {
                $transaction->commit();
                return $this->redirect(['index']);
            }
            else {
                $model->addError('type_id', Yii::t('app', 'Insufficient funds'));
            }
        }

        $user = $model->isNewRecord ? User::findOne($model->user_id) : $model->user;
        /** @var Type[] $types */
        $types = $user->getAvailableTypes()->all();
        $accessible = [];
        foreach($types as $t) {
            $accessible[$t->id] = $t;
        }

        return $this->render('edit', [
            'accessible' => $accessible,
            'model' => $model
        ]);
    }

    public function actionChoose($user_id, $referral_id = null) {
        if ($referral_id) {
            $transaction = Yii::$app->db->beginTransaction();
            /** @var Node $referral_node */
            $referral_node = Node::findOne($referral_id);
            /** @var Node $target */
            $target = Node::findOne((int) $_GET['id']);
            if ($referral_node && $target) {
                if ($target->parent_id) {
                    Yii::$app->session->addFlash('warning', Yii::t('app', 'Parent investment already assign'));
                    return $this->redirect(['choose', 'user_id' => $user_id]);
                }
                $target->parent_id = $referral_node->findFree();
                if ($target->save()) {
                    if ($target->enter(false)) {
                        $transaction->commit();
                        Yii::$app->session->addFlash('success', Yii::t('app', 'Success'));
                    }
                    return $this->redirect(['choose', 'user_id' => $user_id]);
                }
            }
        }

        $query = Node::find()
            ->joinWith('user')
            ->andWhere('parent_id is null')
            ->andWhere('"user".referral_id = :referral_id', [
                ':referral_id' => $user_id
            ]);
        $branches = [];
        $b = SQL::queryAll('SELECT c.id, c.parent_id, c.type_id FROM matrix.node c JOIN matrix.node p
          ON c.parent_id = p.id WHERE p.user_id = :user_id', [
            ':user_id' => $user_id
        ], PDO::FETCH_OBJ);
        foreach($b as $node) {
            $branches[$node->type_id][isset($branches[$node->type_id]) ? 'right' : 'left'] = $node;
        }

        return $this->render('choose', [
            'dataProvider' => new ActiveDataProvider([
                'query' => $query,
                'sort' => [
                    'defaultOrder' => ['id' => SORT_DESC]
                ]
            ]),
            'branches' => $branches
        ]);
    }

    public function actionGraph($id, $depth = 1) {
        if (!Yii::$app->user->can('manage')) {
            Yii::$app->layout = 'cabinet';
        }
        /** @var Node $model */

        $model = $this->findModel($id);
        return $this->render('graph', [
            'model' => $model,
            'root' => $model->tree($depth),
            'depth' => $depth
        ]);
    }

    public function actionAccrue($object_id = null) {
        if (!Yii::$app->user->can('manage')) {
            Yii::$app->layout = 'cabinet';
        }
        $query = Accrue::find()->filterWhere([
            'object_id' => $object_id
        ]);
        return $this->render('accrue', [
            'dataProvider' => new ActiveDataProvider([
                'query' => $query,
                'sort' => [
                    'defaultOrder' => ['time' => SORT_DESC]
                ]
            ]),
            'sum' => $query->sum('amount')
        ]);
    }

    protected function findModel($id) {
        $model = Node::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException();
        }
        return $model;
    }
}

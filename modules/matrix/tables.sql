CREATE SCHEMA matrix;

CREATE TABLE matrix.type (
  id      SMALLINT PRIMARY KEY,
  price   INT  NOT NULL,
  enabled BOOL NOT NULL DEFAULT true
);

INSERT INTO matrix.type(id, price) VALUES (1, 100);
INSERT INTO matrix.type(id, price) VALUES (2, 200);
INSERT INTO matrix.type(id, price) VALUES (3, 300);


CREATE SEQUENCE matrix.node_id_seq
  INCREMENT BY 10
  START 10;

CREATE TABLE matrix.node (
  id        INT PRIMARY KEY DEFAULT nextval('matrix.node_id_seq'),
  type_id   SMALLINT NOT NULL,
  user_id   VARCHAR(24) NOT NULL,
  parent_id INT,
  CONSTRAINT user_id FOREIGN KEY (user_id)
  REFERENCES "user"(id)
  ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT type_id FOREIGN KEY (type_id) REFERENCES matrix.type (id)
  ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT parent_id FOREIGN KEY (parent_id) REFERENCES matrix.node(id)
  ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO matrix.node VALUES
  (1, 1, 'admin', null),
  (2, 2, 'admin', null),
  (3, 3, 'admin', null);

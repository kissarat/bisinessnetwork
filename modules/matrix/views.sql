CREATE VIEW matrix.tree AS
    WITH RECURSIVE r(id, type_id, user_id, "level", root_id, parent_id) AS (
      SELECT
        n.id,
        type_id,
        user_id,
        0    AS "level",
        n.id AS root_id,
        n.parent_id
      FROM matrix.node n
      UNION
      SELECT
        n.id,
        n.type_id,
        n.user_id,
        r."level" + 1 AS "level",
        r.root_id,
        n.parent_id
      FROM matrix.node n
        JOIN r ON r.id = n.parent_id
    )
    SELECT
      r.id,
      r.type_id,
      r.user_id,
      r."level",
      r.root_id,
      r.parent_id,
      t.price * 0.1        AS interest,
      row_number()
      OVER (PARTITION BY root_id
        ORDER BY r.id) - 1 AS "number",
      row_number()
      OVER (PARTITION BY root_id, r."level"
        ORDER BY r.id)     AS "pos"
    FROM r
      JOIN matrix.type t
        ON r.type_id = t.id
    ORDER BY type_id, root_id, r.id;



CREATE VIEW matrix.degree AS
  SELECT root_id, "level", count(*) as count FROM matrix.tree m
    JOIN matrix.type t ON m.type_id = t.id
  GROUP BY root_id, "level" HAVING count(*) < power(2, "level");


CREATE VIEW matrix.referral AS
    WITH RECURSIVE r(id, type_id, user_id, referral_level, sponsor_id, parent_id) AS (
      SELECT
        n.id,
        n.type_id,
        user_id,
        0    AS referral_level,
        n.id AS sponsor_id,
        n.parent_id
      FROM matrix.node n
      UNION
      SELECT
        n.id,
        n.type_id,
        n.user_id,
        r.referral_level + 1 AS referral_level,
        r.sponsor_id,
        n.parent_id
      FROM matrix.node n
        JOIN r ON r.parent_id = n.id
    )
    SELECT
      r.id,
      r.type_id,
      r.user_id,
      r.parent_id,
      price * 0.1 AS interest,
      first_value(r.id) OVER (PARTITION BY sponsor_id ORDER BY r.id) AS root_id,
      row_number() OVER (PARTITION BY sponsor_id ORDER BY r.id) - 1 AS "level",
      r.referral_level,
      r.sponsor_id
    FROM r
      JOIN matrix.type t ON r.type_id = t.id
    ORDER BY sponsor_id;


CREATE VIEW matrix.accrue AS
  SELECT
    t.id,
    t.user_id,
    m.root_id as object_id,
    t.time,
    t.amount,
    m.level
  FROM matrix.tree m JOIN public.transfer t ON m.user_id = t.user_id AND
                                               m.root_id = t.object_id::INTEGER
    JOIN matrix.node n ON t.user_id = n.user_id AND m.type_id = n.type_id
  WHERE t.type = 'node' AND t.event = 'accrue';


CREATE VIEW matrix.income AS
  SELECT object_id as id, sum(amount) as amount FROM matrix.accrue GROUP BY object_id;


<?php
use app\modules\matrix\models\Node;
use yii\grid\GridView;
use yii\helpers\Html;

/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var Node[] $branches */
?>
<div class="node-choose">
    <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'user_id',
        [
            'label' => Yii::t('app', 'Branch'),
            'format' => 'html',
            'value' => function(Node $model) use ($branches) {
                $links = [];
                foreach($branches[$model->type_id] as $name => $branch) {
                    $links[] = Html::a(Yii::t('app', $name), [
                        'choose',
                        'user_id' => $_GET['user_id'],
                        'id' => $model->id,
                        'parent_id' => $branch->parent_id,
                        'referral_id' => $branch->id,
                        'branch' => $name,
                    ]);
                }
                return implode(' ', $links);
            }
        ]
    ]
]) ?>
</div>

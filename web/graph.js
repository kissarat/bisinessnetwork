"use strict";

var root = null;

function build_graph(source) {
    var nodes = tree.nodes(root).reverse();

    nodes.forEach(function(d) {
        d.y = d.depth * 100;
        d.x *= 2;
    });

    var links = tree.links(nodes);

    var node = svg.selectAll("g.node")
        .data(nodes, function(d) { return d.id || (d.id = ++i); });

    var nodeEnter = node.enter().append("g")
        .attr("class", "node")
        .attr("transform", function(d) {
            return "translate(" + d.x + "," + d.y + ")"; });

   var img = nodeEnter.append("image")
        .attr('height', 74.755234)
        .attr('width', 43.73925)
        .attr('transform', function(d) { return 'translate(-21.5,-60)' })
        .attr("xlink:href",function(d) {return "/images/node"+ d.type_id+".png"; });
    img.attr('class',function(d) { return "class"+d.type_id; });

    nodeEnter.append("text")
        .text(function(d) { return d.name; })
        .attr('transform', 'translate(0, 28)')
        .attr('text-anchor', 'middle');

    var link = svg.selectAll("path.link")
        .data(links, function(d) { return d.target.id; });

    link.enter().insert("path", "g")
        .attr("class", "link")
        .attr("d", diagonal);
}

var margin = {top: 80, right: 20, bottom: 20, left: 20},
    width = innerWidth - margin.right - margin.left,
    height = innerHeight - margin.top - margin.bottom;

var i = 0;

var tree = d3.layout.tree()
    .size([height, width]);

var diagonal = d3.svg.diagonal()
    .projection(function(d) { return [d.x, d.y]; });

var svg = d3.select("article").append("svg")
    .attr("width", width + margin.right + margin.left)
   .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
$(document).scroll(function(){
   if($("#globus").height() >= $("#youtube").height() && $("#globus").height() >= $("#money").height()){
      // alert("globus");
       var height = $("#globus").height();
       var margin = (height - $("#youtube").height())/2;
       $("#youtube").css("margin-top",margin);
       margin = (height - $("#money").height())/2;
       $("#money").css("margin-top",margin);
   } else if($("#youtube").height() >= $("#globus").height() && $("#youtube").height() >= $("#money").height()){
      // alert("youtube");
       var height = $("#youtube").height();
       var margin = (height - $("#globus").height())/2;
       $("#globus").css("margin-top",margin);
       margin = (height - $("#money").height())/2;
       $("#money").css("margin-top",margin);
   } else if($("#money").height() >= $("#youtube").height() && $("#money").height() >= $("#globus").height()){
     //  alert("money");
       var height = $("#money").height();
       var margin = (height - $("#youtube").height())/2;
       $("#youtube").css("margin-top",margin);
       margin = (height - $("#globus").height())/2;
       $("#globus").css("margin-top",margin);
   }

});
function getRate(from, to) {
    var script = document.createElement('script');
    script.setAttribute('src', "http://query.yahooapis.com/v1/public/yql?q=select%20rate%2Cname%20from%20csv%20where%20url%3D'http%3A%2F%2Fdownload.finance.yahoo.com%2Fd%2Fquotes%3Fs%3D"+from+to+"%253DX%26f%3Dl1n'%20and%20columns%3D'rate%2Cname'&format=json&callback=parseExchangeRate");
    document.body.appendChild(script);
}
function parseExchangeRate(data) {
    var name = data.query.results.row.name;
    name = name.replace('/','');
    var rate = parseFloat(data.query.results.row.rate, 10);
    $("#money #"+name).html(rate);
    //alert("Exchange rate " + name + " is " + rate);
}
$(document).ready(function(){
    if(!$(".article.top").length) {
        if ($(".main-layout").length) {
            var gradient = $(".panel-heading").height();
            var panel = $(".panel-heading");
            var ofset = panel.offset();
            var carousel = $("#carousel-example-generic").height();
            var needwindow = $(window).height() - gradient;
            var nec = needwindow - ofset.top;
            var readyheigh = carousel + nec - 6;
            $(".carousel-inner > .item > img").height(readyheigh);
            $("#carousel-example-generic").width($(".navbar-nav.nav#w1").width() - 20);
        }
        getRate("USD", "KZT");
        getRate("USD", "AZN");
        getRate("USD", "UZS");
        getRate("USD", "EUR");
        getRate("USD", "GBP");
        getRate("USD", "BYR");
        getRate("USD", "UAH");
    }

        if ($(".main-layout").length) {
            moment.locale('ru');
            setInterval(function () {
                $("#top-date").html(moment().format('MMMM Do YYYY, h:mm:ss a'));
            }, 1000);
        }


});

$(".node.graph article svg").ready(function(){
    var hg = $(".node.graph article").attr('dep');
   // alert(hg);
    $(".node.graph article svg").attr('height', hg*100+150);
});

$('select[placeholder]').each(function(i, select) {
    $('<option />')
        .text(select.getAttribute('placeholder'))
        .attr('disabled', true)
        .attr('selected', true)
        .prependTo(select);
});
$('[name=User]').on('beforeSubmit', function(e) {
    if (!window.agree || agree.checked) {
        return true;
    }
    alert('You must confirm agreement');
    return false;
});

addEventListener('click', function() {
    $each('.float-menu.active', function(active) {
        active.classList.remove('active');
    });
});

$('#menu')
    .find('dl')
    .addClass('article-content')
    .appendTo('#menu');

$each('.collapsible dt', function(dt) {
    var dd = dt.nextElementSibling;
    dt.onmouseover = function() {
        $each('.float-menu.active', function(active) {
            active.classList.remove('active');
        });
        if (!this.$dd) {
            return;
        }
        dd.classList.add('active');
        this.addEventListener('mouseout', autoremove);
        window.document.body.addEventListener('click', function() {
            this.classList.remove('active');
            this.removeEventListener('mouseout', autoremove);
        });
    };
    if ('DD' != dd.tagName) {
        return;
    }

    dd.remove();
    dt.$dd = dd = $new('div', {class: 'float-menu panel'}, dd.innerHTML);
    //    [/
    //    $new('div', {class: 'panel-heading'}),
    //    $new('div', {class: 'panel-body'}, dd.innerHTML),
    //    $new('div', {class: 'panel-footer'})
    //]);
    dd.classList.add('float-menu');
    var box = dt.getBoundingClientRect();
    dd.style.top = box.top + 'px';
    dd.style.left = box.right + 'px';
    document.body.appendChild(dd);
});

var ticket = $('#ticket');
ticket.find('button').click(function () {
    window.location = '/ticket/' + ticket.find('input').val();
});

var category_id_input = $$('#article-parent_id');
if (category_id_input) {
    var category_search_input = $new('input', {
        type: 'search'
    });
    var category_list = $new('div');
    var category_title = $id('category_title');
    category_title.remove();
    category_search_input.onkeyup = function() {
        $.getJSON('/article/search?title=' + this.value, function(titles) {
            category_list.innerHTML = '';
            for(var id in titles) {
                var item = $new('div', {'data-id': id}, titles[id]);
                item.onclick = function() {
                    category_id_input.value = this.dataset.id;
                    category_title.innerHTML = this.innerHTML;
                };
                category_list.appendChild(item);
            }
        });
    };
    category_id_input.visible = false;
    var category_search = $new('div', {id:'category_search'}, [
        category_search_input,
        category_list
    ]);
    $add(category_id_input.parentNode,
        category_title,
        category_search
    );
}

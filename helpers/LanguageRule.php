<?php

namespace app\helpers;


use Yii;
use yii\helpers\Url;
use yii\web\UrlRuleInterface;

class LanguageRule implements UrlRuleInterface
{
    public function parseRequest($manager, $request)
    {
        return false;
    }

    public function createUrl($manager, $route, $params)
    {
        if (('article/index' == $route || 'article/view' == $route) && 1 == count($params)) {
            $params['lang'] = Yii::$app->language;
            return Url::toRoute($route, $params);
        }
        return false;
    }
}

<?php

namespace app\helpers;


use Yii;

abstract class PerfectHelper
{
    public static function validate($wallet, $secret) {
        $fields = [
            'PAYMENT_ID' => '/^\d+$/',
            'PAYEE_ACCOUNT' => '/^U\d{7,8}$/',
            'PAYMENT_AMOUNT' => '/^\d+(\.\d{2})?$/',
            'PAYMENT_UNITS' => '/^USD$/',
            'PAYMENT_BATCH_NUM' => '/^\d+$/',
            'PAYER_ACCOUNT' => '/^U\d{7,8}$/',
            'TIMESTAMPGMT' => '/^\d+$/',
            'V2_HASH' => '/^[0-9A-Z]+$/',
        ];
        foreach($fields as $name => $pattern) {
            if (empty($_POST[$name]) || !preg_match($pattern, $_POST[$name])) {
                return false;
            }
        }

        if ($wallet != $_POST['PAYEE_ACCOUNT']) {
            Yii::$app->session->addFlash('error', Yii::t('app', 'Invalid payee'));
            return false;
        }

        $string = implode(':', [
            $_POST['PAYMENT_ID'],
            $_POST['PAYEE_ACCOUNT'],
            $_POST['PAYMENT_AMOUNT'],
            $_POST['PAYMENT_UNITS'],
            $_POST['PAYMENT_BATCH_NUM'],
            $_POST['PAYER_ACCOUNT'],
            strtoupper(md5($secret)),
            $_POST['TIMESTAMPGMT']
        ]);

        if ($_POST['V2_HASH'] != strtoupper(md5($string))) {
            Yii::$app->session->addFlash('error', Yii::t('app', 'Invalid hash'));
            return false;
        }

    }
}

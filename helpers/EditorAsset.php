<?php

namespace app\helpers;


class EditorAsset extends MainAsset
{
    public $js = [
        'ckeditor.js',
        'youtube.js'
    ];
}

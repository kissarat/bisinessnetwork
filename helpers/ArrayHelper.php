<?php

namespace app\helpers;


use yii\helpers\BaseArrayHelper;

abstract class ArrayHelper extends BaseArrayHelper
{
    public static function concat($target, $attributes) {
        if (null == $target) {
            $target = [];
        }
        foreach($attributes as $key => $value) {
            if (empty($target[$key])) {
                $target[$key] = $value;
            }
            else {
                $target[$key] .= $value;
            }
        }
        return $target;
    }

    public static function filter($keys, $source = null) {
        if (null === $source) {
            $source = &$_GET;
        }
        $target = [];
        foreach($keys as $key) {
            if(isset($source[$key])) {
                $target[$key] = $source[$key];
            }
        }
        return $target;
    }

    public static function equalsAny($keys, $value, $source = null) {
        if (null === $source) {
            $source = &$_GET;
        }
        foreach($keys as $key) {
            if(isset($source[$key]) && $value == $source[$key]) {
                return true;
            }
        }
        return false;
    }
}

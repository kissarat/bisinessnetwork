<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $subject string main view render result */
/* @var $content string main view render result */

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
<head>
    <meta name="charset" content="<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($subject) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php
$this->beginBody();
echo $content;
$this->endBody();
?>
</body>
</html>
<?php $this->endPage() ?>

<?php

namespace app\models;


use Yii;

/**
 * @property string parent_id
 * @property string title
 * @property string keywords
 * @property string summary
 * @property number price
 */
class Article extends Text
{
    public $image;
    public $video;

    public static function tableName()
    {
        return 'article';
    }

    public function rules()
    {
        return [
            [['title'], 'required'],
            ['id', 'string'],
            ['title', 'string', 'min' => 3, 'max' => 255],
            ['keywords', 'string', 'min' => 3, 'max' => 192],
            ['summary', 'string', 'min' => 3, 'max' => 192],
            ['content', 'string', 'min' => 20],
            ['image', 'file'],
            ['price', 'number'],
            ['parent_id', 'string'],
            ['parent_id', 'exist',
                'targetClass' => 'app\models\Article',
                'targetAttribute' => 'id',
                'message' => Yii::t('app', 'The category does not exists')],
            [['id', 'keywords', 'summary', 'content', 'parent_id'], 'default', 'value' => null]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'lang' => Yii::t('app', 'Language'),
            'title' => Yii::t('app', 'Title'),
            'keywords' => Yii::t('app', 'Keywords'),
            'summary' => Yii::t('app', 'Summary'),
            'content' => Yii::t('app', 'Content'),
            'image' => Yii::t('app', 'Image'),
            'parent_id' => Yii::t('app', 'Category'),
            'price' => Yii::t('app', 'Price'),
        ];
    }

    public function getMetaTags() {
        $tags = [];
        if ($this->keywords) {
            $tags['keywords'] = $this->keywords;
        }
        if ($this->summary) {
            $tags['description'] = $this->summary;
        }
        return $tags;
    }

    public function __toString()
    {
        return $this->title;
    }

    public static function findOne($condition) {
        if (is_string($condition)) {
            $condition = [
                'id' => $condition,
                'lang' => Yii::$app->language
            ];
        }

        $model = parent::findOne($condition);
        if (!$model) {
            $model = new Article($condition);
        }

        return $model;
    }

    public static function widget($condition) {
        return Yii::$app->view->render('@app/views/article/view', [
            'model' => static::findOne($condition),
            'condition' => $condition,
            'meta' => false
        ]);
    }

    public function safeId() {
        return str_replace('/', '-', $this->id);
    }

    public function getParent() {
        return $this->hasOne(Article::className(), [
            'id' => 'parent_id',
            'lang' => 'lang'
        ]);
    }

    public function getCategories() {
        return $this->hasMany(Category::className(), [
            'category_id' => 'id',
            'lang' => 'lang'
        ]);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * @property integer $id
 * @property string $user_id
 * @property string $type
 * @property number $amount
 * @property integer $number
 * @property integer $batch
 * @property string $status
 * @property string $wallet
 * @property boolean $withdraw
 *
 * @property User $user
 */
class Invoice extends UserRecord
{
    public static $statuses = [
        'create' => 'Created',
        'invalid_amount' => 'Invalid amount',
        'invalid_receiver' => 'Invalid receiver',
        'invalid_batch' => 'Transaction ID does not match',
        'invalid_response' => 'Unknown server response',
        'invalid_id' => 'Invalid ID',
        'invalid_hash' => 'Invalid hash',
        'invalid_currency' => 'Invalid currency',
        'insufficient_funds' => 'Insufficient funds in the account user',
        'no_qualification' => 'User does not qualify',
        'cancel' => 'Cancel',
        'fail' => 'Error',
        'success' => 'Completed',
        'verified' => 'Проверен',
    ];

    public static $types = [
        'perfect' => 'Perfect Money',
//        'nix' => 'NixMoney',
//        'advcash' => 'AdvCash'
    ];

    public static function listTypes() {
        $types = static::$types;
        $common = Config::getCategory('common');
        foreach($types as $key => $value) {
            if (isset($types[$key]) && !$common[$key]) {
                unset($types[$key]);
            }
        }
        return $types;
    }

    public static function tableName() {
        return 'invoice';
    }

    public function traceable() {
        return ['status'];
    }

    public function rules() {
        return [
            [['user_id', 'amount'], 'required'],
            [['user_id', 'status'], 'string', 'max' => 16],
            ['amount', 'number'],
            ['withdraw', 'boolean'],
            ['batch', 'string'],
            ['wallet', 'string'],
            ['user_id', 'default', 'value' => Yii::$app->user->id]
        ];
    }

    public function scenarios()
    {
        return [
            'default' => ['user_id', 'type', 'amount', 'wallet', 'batch', 'withdraw'],
            'manage' =>  ['user_id', 'type', 'amount', 'wallet', 'batch', 'withdraw', 'status'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'Username'),
            'amount' => Yii::t('app', 'Amount'),
            'status' => Yii::t('app', 'Status'),
            'wallet' => Yii::t('app', 'Wallet'),
            'batch' => Yii::t('app', 'Transaction ID'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

    public function saveStatus($status) {
        $this->status = $status;
        return $this->save();
    }

    public function __toString() {
        $text = $this->wallet
            ? 'Withdraw #{id} for ${amount}'
            : 'Payment #{id} for ${amount}';
        return Yii::t('app', $text, [
            'id' => $this->id,
            'amount' => abs($this->amount),
        ]);
    }
}

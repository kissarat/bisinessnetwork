<?php
/**
 * Created by PhpStorm.
 * User: yura
 * Date: 17.02.16
 * Time: 11:15
 */
namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "posts".
 *

 * @property string $user_id
 * @property string $content
 *
 * @property User $user
 */
class Capture extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'capture';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'content'], 'required'],
            [['user_id','content'], 'string'],
        ];
    }

    public static function primaryKey() {
        return ['user_id'];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app','user'),
            'content' => Yii::t('app','Content'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_id']);
    }
}
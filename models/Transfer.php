<?php

namespace app\models;
use Yii;

/**
 * @property integer $id
 * @property string $type
 * @property string $event
 * @property string $user_id
 * @property string $object_id
 * @property string $ip
 * @property string $time
 * @property number $amount
 *
 * @property User receiver
 */
class Transfer extends UserRecord
{
    public function rules() {
        return [
            [['type', 'event', 'user_id', 'object_id', 'ip', 'time', 'receiver_id'], 'string'],
            ['amount', 'number']
        ];
    }

    public static function tableName()
    {
        return 'transfer';
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'user_id' =>  Yii::t('app', 'Investor'),
            'time' => Yii::t('app', 'Time'),
            'receiver_id' => Yii::t('app', 'Receiver'),
        ];
    }

    public function getReceiver_id() {
        return $this->object_id;
    }

    public function setReceiver_id($value) {
        $this->object_id = $value;
    }

    public function getReceiver() {
        return $this->hasOne(User::className(), ['id' => 'object_id']);
    }
}

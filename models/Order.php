<?php

namespace app\models;
use Yii;

/**
 * @property string id
 * @property string email
 * @property string forename
 * @property string surname
 * @property string patronymic
 * @property integer phone
 * @property string skype
 * @property string country
 * @property string postal
 * @property string city
 * @property string address
 * @property string user_id
 * @property integer product_id
 * @property integer invoice_id
 *
 * @property User user
 * @property Article product
 * @property Invoice invoice
 */
class Order extends UserRecord
{
    public function rules() {
        return [
            [['forename', 'surname',
                'patronymic', 'phone', 'country', 'postal', 'city', 'address'], 'required'],
            [['skype', 'id'], 'string'],
            ['email', 'email'],
            ['user_id', 'exist',
                'targetClass' => 'app\models\User',
                'targetAttribute' => 'id',
                'message' => Yii::t('app', 'This value has already been taken')],
            ['invoice_id', 'exist',
                'targetClass' => 'app\models\Invoice',
                'targetAttribute' => 'id',
                'message' => Yii::t('app', 'The invoice does not exists')],
            ['product_id', 'exist',
                'targetClass' => 'app\models\Article',
                'targetAttribute' => 'id',
                'message' => Yii::t('app', 'The product does not exists')],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => Yii::t('app', 'Email'),
            'forename' => Yii::t('app', 'Forename'),
            'surname' => Yii::t('app', 'Surname'),
            'patronymic' => Yii::t('app', 'Patronymic'),
            'phone' => Yii::t('app', 'Phone'),
            'skype' => Yii::t('app', 'Skype'),
            'country' => Yii::t('app', 'Country'),
            'postal' => Yii::t('app', 'Postal'),
            'city' => Yii::t('app', 'City'),
            'address' => Yii::t('app', 'Address'),
            'invoice_id' => Yii::t('app', 'Invoice'),
            'product_id' => Yii::t('app', 'Product'),
            'user_id' => Yii::t('app', 'User'),

            'title' => Yii::t('app', 'Title'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    public function getProduct() {
        return $this->hasOne(Article::className(), ['id' => 'product_id']);
    }

    public function getInvoice() {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }
}

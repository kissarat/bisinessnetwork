<?php

require_once 'pdo.php';

$name = $config['components']['db']['username'];
$password = $config['components']['db']['password'];


$sock = file_exists('/var/run/postgresql') ? '/var/run/postgresql' : '/tmp';
if (!file_exists($sock)) {
    die('PostgreSQL socket file does not exists');
}

if (!pg_connect('host=' . $sock)) {
    pg_connect('host=' . $sock . ' user=postgres password=1');
}

try {
    pg_query("DROP DATABASE \"$name\"");
}
catch (Exception $ex) {
    echo $ex->getMessage();
}

try {
    pg_query("DROP ROLE \"$name\"");
}
catch (Exception $ex) {
    echo $ex->getMessage();
}

pg_query("CREATE ROLE \"$name\" LOGIN PASSWORD '$password'");
pg_query("CREATE DATABASE \"$name\" OWNER \"$name\" CONNECTION LIMIT = 10");

pg_close();

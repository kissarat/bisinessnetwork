<?php
require_once 'pdo.php';
#require_once 'reset.php';

$schema = file_get_contents(ROOT . '/config/tables.sql');
$schema = explode(';', $schema);
$views = file_get_contents(ROOT . '/config/rbac.sql');
$schema = array_merge($schema, explode(';', $views));
$views = file_get_contents(ROOT . '/config/views.sql');
$schema = array_merge($schema, explode(';', $views));
//$schema[] = file_get_contents(ROOT . '/config/procedure.sql');
//$schema[] = file_get_contents(ROOT . '/data/01-source_message.sql');
//$schema[] = file_get_contents(ROOT . '/data/02-message.sql');

define('MODULES', __DIR__ . '/../modules');
$dir = opendir(MODULES);
while ($name = readdir($dir)) {
    if ('.' != $name[0]) {
        $sql_files = ['schema.sql', 'tables.sql', 'views.sql', 'functions.sql'];
        foreach($sql_files as $file) {
            $path = MODULES . "/$name/$file";
            if (file_exists($path)) {
                echo "$name/$file" . "\n";
                $schema[] = file_get_contents($path);
            }
        }
    }
}

$pdo = connect();
$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$pdo->beginTransaction();
foreach($schema as $sql) {
    $sql = trim($sql);
//    if (0 === strpos($sql, '--')) {
//        continue;
//    }
    if (!empty($sql)) {
        $sql = preg_replace('|/\*[^\*]+\*/|', '', $sql);
        $sql = preg_replace('|[\t ]+|', ' ', $sql);
        $sql = trim($sql);
//        echo $sql . "\n";
        $pdo->exec($sql);
    }
}
$pdo->commit();
apc_clear_cache('user');

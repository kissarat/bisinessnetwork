<?php

namespace app\widgets;


use app\helpers\ArrayHelper;
use yii\bootstrap\Html;

class Panel extends Article
{
    public function items() {
        $this->options = ArrayHelper::concat($this->options, [
            'class' => ' panel panel-default'
        ]);
        $this->titleOptions = ArrayHelper::concat($this->titleOptions, [
            'class' => ' panel-heading'
        ]);
        $this->bodyOptions = ArrayHelper::concat($this->bodyOptions, [
            'class' => ' panel-body'
        ]);

        $items = parent::items();
        $items[] = Html::tag('div', '', ['class' => 'panel-footer']);
        return $items;
    }
}

<?php

namespace app\widgets;


use Yii;
use yii\base\Widget;
use yii\helpers\Html;

class Fake extends Widget
{
    public function run() {
        echo Html::button(Yii::t('app', 'Fake'), [
            'onclick' => 'fake()',
            'class' => 'button'
        ]);
    }
}

<?php

namespace app\widgets;


use app\models\Article as ArticleModel;
use InvalidArgumentException;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;

class Article extends Widget
{
    public $id;
    public $title;
    public $options;
    public $titleOptions;
    public $bodyOptions;
    public $model;

    /**
     * @return string[]
     */
    protected function items() {
        /** @var ArticleModel $model */
        $model = null;
        if (empty($this->id)) {
            if (empty($this->model)) {
                $model = new ArticleModel();
            }
            elseif (is_array($this->model)) {
                $model = new ArticleModel($this->model);
            }
            elseif (is_string($this->model)) {
                $model = new ArticleModel(['content' => $this->model]);
            }
            elseif($this->model instanceof ArticleModel) {
                $model = $this->model;
            }
            else {
                throw new InvalidArgumentException('model');
            }
        }
        else {
            $model = ArticleModel::findOne($this->id);
        }
        $items = [];
        if (false !== $this->title) {
            $items[] = Html::tag('div', is_string($this->title) ? $this->title : $model->title, $this->titleOptions);
        }
        $body = [];
        $url = null;
        if ($this->id) {
            $url = is_array($this->id)
                ? array_merge(['/article/edit'], $this->id)
                : ['/article/edit', 'id' => $this->id];
            if (Yii::$app->user->can('manage')) {
                $body[] = Html::a(Yii::t('app', 'Edit'), array_merge(['/article/edit'], $url));
            }
        }
        if ($model->content) {
            if ($this->id) {
                $content = strip_tags(substr(Html::tag('figure', Html::img('/data/article/thumb/' . $model->safeId() . '.jpg'))
                    . ' ' . $model->content,0,801)."...",'<p><strong><br>')
                    . ' ' . Html::a(Yii::t('app', 'Read'), str_replace('edit', 'view', $url));
            }
            else {
                $content = $model->content;
            }
            $body[] = Html::tag('div', $content, ['class' => 'article-content']);
        }
        $items[] = Html::tag('div', implode("\n", $body), $this->bodyOptions);
        return $items;
    }

    public function run() {
        $items = $this->items();
        $options = empty($this->id)
            ? $this->options
            : array_merge(['id' => $this->id], $this->options);
        echo Html::tag('div', implode("\n", $items), $options);
    }
}

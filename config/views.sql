CREATE VIEW tree AS
  WITH RECURSIVE r(id, referral_id, root_id, level) AS (
    SELECT
      id,
      referral_id,
      id AS root_id,
      0 as level
    FROM "user"
    UNION
    SELECT
      u.id,
      u.referral_id,
      r.root_id,
      r.level + 1 as level
    FROM "user" u JOIN r ON u.referral_id = r.id
  )
  SELECT
    id,
    referral_id,
    root_id,
    level
  FROM r;

CREATE OR REPLACE VIEW category AS
  WITH RECURSIVE r(id, title, lang, category_level, category_id, parent_id, price) AS (
    SELECT
      a.id,
      a.title,
      a.lang,
      0    AS category_level,
      a.id AS category_id,
      a.parent_id,
      price
    FROM article a
    UNION
    SELECT
      a.id,
      a.title,
      a.lang,
      r.category_level + 1 AS category_level,
      r.category_id,
      a.parent_id,
      a.price
    FROM article a
      JOIN r ON r.parent_id = a.id AND r.lang = a.lang
    WHERE r.category_level < 8
  )
  SELECT
    r.id,
    r.title,
    r.lang,
    r.parent_id,
    first_value(r.id) OVER (PARTITION BY category_id ORDER BY r.id) AS root_id,
    row_number() OVER (PARTITION BY category_id ORDER BY r.id) - 1 AS "level",
    r.category_level,
    r.category_id,
    price
  FROM r
  ORDER BY category_level DESC;


CREATE OR REPLACE VIEW article_view AS
  SELECT p.*, count(c.id) as count FROM article p LEFT JOIN article c
      ON p.id = c.parent_id AND p.lang = c.lang
  GROUP BY p.id, p.lang, p.content, p.parent_id, p.number,
  p.title, p.keywords, p.summary;


CREATE OR REPLACE VIEW article_tree AS
  WITH RECURSIVE r(id, lang, title, summary, content, parent_id, root_id, level, number, count, price) AS (
    SELECT
      id,
      lang,
      title,
      summary,
      content,
      parent_id,
      id AS root_id,
      0 as level,
      number,
      count,
      price
    FROM article_view
    UNION
    SELECT
      a.id,
      a.lang,
      a.title,
      a.summary,
      a.content,
      a.parent_id,
      r.root_id,
      r.level + 1 as level,
      a.number,
      a.count,
      a.price
    FROM article_view a JOIN r ON a.parent_id = r.id AND r.lang = a.lang
    WHERE r.level < 8
  )
  SELECT
    id,
    lang,
    title,
    coalesce(summary, substring(content from 0 for 320)) as summary,
    parent_id,
    root_id,
    level,
    number,
    count,
    price
  FROM r
  WHERE level > 0;


CREATE VIEW order_view AS
  SELECT
    o.id,
    invoice_id,
    status,
    o.user_id,
    product_id,
    title,
    i.amount as price
  FROM "order" o JOIN invoice i ON invoice_id = i.id
  JOIN "user" u ON o.user_id = u.id
  JOIN article a ON product_id = a.id;


CREATE VIEW hit AS
  SELECT
    a.id, a.title, a.lang, a.price,
    count(o.id) as count
  FROM "order" o JOIN invoice i ON invoice_id = i.id
  RIGHT JOIN article_view a ON product_id = a.id
  WHERE a.price is not null AND i.status = 'success' AND a.count <= 0
  GROUP BY a.id, a.title, a.lang, a.price, a.count;
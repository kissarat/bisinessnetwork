<?php

define('ROOT', __DIR__ . '/..');

$config = [
    'id' => 'bisinessnetwork',
    'name' => 'Yiibut',
    #'timeZone' => 'Europe/Moscow',
    'basePath' => __DIR__ . '/..',
    'bootstrap' => ['log'],
    'defaultRoute' => 'home/index',
    'charset' => 'utf-8',
    'layout' => isset($_COOKIE['role']) && 'god' == $_COOKIE['role'] ? 'admin' : 'main',
];

$config['components'] = [
    'db' => [
        'class' => 'yii\db\Connection',
        'dsn' => 'pgsql:host=localhost;dbname=' . $config['id'],
        'username' => $config['id'],
        'password' => $config['id'],
        'charset' => 'utf8',
        'enableSchemaCache' => true
    ],

    'mailer' => [
        'class' => 'yii\swiftmailer\Mailer',
        'transport' => [
            'class' => 'Swift_SmtpTransport',
            'host' => 'smtp.yandex.ru',
            'username' => 'kissarat@yandex.ru',
            'password' => 'kuro4ka',
            'port' => 465,
            'encryption' => 'ssl',

        ],
    ],

    'authManager' => [
        'class' => 'yii\rbac\DbManager',
    ],

    'local' => [
        'class' => 'creocoder\flysystem\LocalFilesystem',
        'path' => '@app',
    ],

    'dropbox' => [
        'class' => 'creocoder\flysystem\DropboxFilesystem',
        'app' => 'ukraine',
        'token' => 'vuQQbUOYPmsAAAAAAAAAdvILbIybqNrgo-cRc_SDarq1dn9wfVljTd3CwFjN6arq',
    ],
];


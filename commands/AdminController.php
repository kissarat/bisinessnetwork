<?php

namespace app\commands;


use app\models\Article;
use app\models\Config;
use app\models\User;
use app\modules\matrix\models\Node;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Image\Point;
use Yii;
use yii\console\Controller;
use yii\imagine\Image;

class AdminController extends Controller {
    public function actionCreate() {
        umask(0000);
        $list = ['runtime', 'web/assets', 'dump',
            'log/perfect/success',
            'web/data/text/ru',
            'web/data/text/en',
            'web/data/article/image',
            'web/data/article/thumb',
            'web/data/feedback'
        ];
        foreach($list as $dir) {
            Yii::$app->local->createDir($dir);
            chmod($dir, 0777);
        }
    }

    public function actionDelete() {
        foreach(['runtime', 'web/assets', 'log'] as $dir) {
            if (is_dir(Yii::getAlias('@app/' . $dir))) {
                Yii::$app->local->deleteDir($dir);
            }
        }
    }

    public function actionInit() {
        $this->actionDelete();
        $this->actionCreate();
    }

    public function actionUser($id, $password = 1) {
        $user = new User([
            'id' => $id,
            'email' => $id . '@yopmail.com'
        ]);
        $user->setPassword($password);
        if (!$user->save()) {
            echo json_encode($user->getErrors(), JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE) . "\n";
        }
    }

    public function actionPassword($user_id, $password = 1) {
        /** @var User $user */
        $user = User::findOne($user_id);
        $user->setPassword($password);
        $user->save(true, ['hash']);
    }

    public function actionGet($category, $name) {
        echo Config::get($category, $name) . "\n";
    }

    public function actionSet($category, $name, $value) {
        Config::set($category, $name, $value);
    }

    public function actionTest($c) {
        $referral_id = 'admin';
        $users = [];
        for($i = 1; $i <= $c; $i++) {
            $id = 'user' . $i;
            $user = new User([
                'id' => $id,
                'email' => $id . '@yopmail.com',
                'referral_id' => $referral_id
            ]);
            if ($user->save(false)) {
                $users[] = $user;
            }
            $referral_id = $id;
        }

        $parent_id = 1;
        foreach($users as $user) {
            $node = new Node([
                'user_id' => $user->id,
                'type_id' => 1,
                'parent_id' => $parent_id
            ]);
            if ($node->save(false)) {
                $parent_id = $node->id;
            }
        }
    }

    public function actionRename($source) {
        $dir = opendir($source);
        $files = [];
        while($file = readdir($dir)) {
            if ('.' != $file[0]) {
                $files[] = $file;
            }
        }
        shuffle($files);
        $i = 1;
        foreach($files as $file) {
            try {
                $image = Image::getImagine()->open("$source/$file");
                $target = Yii::getAlias("@app/web/data/article/image/$i.jpg");
                copy("$source/$file", $target);
                $size = $image->getSize();
                $min = min($size->getWidth(), $size->getHeight());
                $max = max($size->getWidth(), $size->getHeight());
                $offset = round(($max - $min) / 2);
                $start = $size->getWidth() > $size->getHeight() ? new Point($offset, 0) : new Point(0, $offset);
                $image->crop($start, new Box($min, $min));
                $image->resize(new Box(128, 106), ImageInterface::FILTER_CUBIC);
                $image->save(Yii::getAlias("@app/web/data/article/thumb/$i.jpg"));
                echo "$i\t\t$file\n";
            }
            catch(\Exception $ex) {
                echo "$i\t" . $ex->getMessage() . "\n";
            }
            $i++;
        }
    }
}

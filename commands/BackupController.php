<?php

namespace app\commands;


use League\Flysystem\MountManager;
use Yii;
use yii\console\Controller;

class BackupController extends Controller {
    public function actionUp() {
        echo get_class(Yii::$app->dropbox->filesystem);
        $god = new MountManager([
            'local' => Yii::$app->local,
            'backup' => Yii::$app->dropbox->filesystem
        ]);
        $god->copy('local://web/images', 'backup://images');
    }

    public function actionExport() {
        $filename = date('y_m_d-H_i') . '.sql';
        $dumppath = Yii::getAlias("@app/dump/$filename");
        $domain = Yii::$app->id;
        shell_exec("pg_dump $domain > $dumppath");
        shell_exec("gzip $dumppath");
        Yii::$app->dropbox->write("$domain/$filename.gz", file_get_contents("$dumppath.gz"));
    }
}

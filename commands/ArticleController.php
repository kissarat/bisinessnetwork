<?php

namespace app\commands;


use app\helpers\SQL;
use app\models\Article;
use Yii;
use yii\console\Controller;

class ArticleController extends Controller
{
    public function actionLoad()
    {
        $posts = require Yii::getAlias('@app/web/data/post.php');
        $i = 0;
        foreach($posts as $post) {
            $article = new Article([
                'id' => (string) ++$i,
                'lang' => 'ru',
                'title' => $post['title'],
                'content' => $post['content'],
            ]);
            if (!$article->save()) {
                die(json_encode($article->getErrors(), JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE) . "\n");
            }
        }
    }

    public function actionCategories() {
        $news = static::article(['id' => 'news', 'title' => 'Новости']);
        $items = [];
        if($news->save()) {
            $items = array_merge($items, [
                static::article(['id' => 'health',   'title' => 'Здоровье',    'parent_id' => $news->id]),
                static::article(['id' => 'business', 'title' => 'Бизнес',      'parent_id' => $news->id]),
                static::article(['id' => 'events',   'title' => 'Мероприятия', 'parent_id' => $news->id]),
                static::article(['id' => 'updates',  'title' => 'Обновления',  'parent_id' => $news->id]),
                static::article(['id' => 'society',  'title' => 'Общество',    'parent_id' => $news->id])
            ]);
        }
        $shop = static::article(['id' => 'shop', 'title' => 'Магазин']);
        if ($shop->save()) {
            $items = array_merge($items, [
                static::article(['id' => 'notebook', 'title' => 'Ноутбуки', 'parent_id' => $shop->id]),
                static::article(['id' => 'book', 'title' => 'Электронные книги', 'parent_id' => $shop->id]),
                static::article(['id' => 'camera', 'title' => 'Фотокамеры', 'parent_id' => $shop->id]),
                static::article(['id' => 'payer', 'title' => 'Плееры', 'parent_id' => $shop->id]),
                static::article(['id' => 'console', 'title' => 'Игровые приставки', 'parent_id' => $shop->id]),
            ]);
        }
        foreach($items as $item) {
            $item->save();
        }
    }

    public function actionCategorize() {
        $count = Article::find()->count();
        static::categorize('news', $count/2);
        static::categorize('shop', 0, function($article) {
            $article->price = 10 * rand(1, 100);
        });
        SQL::execute("UPDATE article SET parent_id = NULL WHERE id IN ('news', 'shop')");
    }

    protected static function categorize($root, $count, $call = null) {
        $categories = Article::find()
            ->where("parent_id = '$root'")
            ->select('id')
            ->orderBy('random()')
            ->column();
        $articles = Article::find()
            ->where("id <> '$root' and parent_id is null")
            ->orderBy('random()');
        if ($count > 0) {
            $articles->limit(ceil($count));
        }
        /** @var Article $article */
        foreach($articles->each() as $article) {
            $article->parent_id = $categories[array_rand($categories)];
            if ($call) {
                call_user_func($call, $article);
            }
            $article->save();
        }
    }

    protected static function article($params) {
        return new Article(array_merge([
            'lang' => 'ru',
            'content' => ''
        ], $params));
    }
}
